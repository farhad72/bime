package clas;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import android.os.AsyncTask;
import com.androidbegin.sidemenutabstutorial.astalam_bimename;


public class clas_astalam_bimename extends AsyncTask {

    private String Link        = "";
    private String Namefamily  = "";
    private String Phnumber    = "";
    private String Nobimname   = "";
    private String Discription = "";


    public clas_astalam_bimename(String link, String name_family, String ph_number, String no_bimname, String discription) {
        Link = link;
        Namefamily = name_family;
        Phnumber = ph_number;
        Nobimname = no_bimname;
        Discription = discription;

    }


    @Override
    protected String doInBackground(Object... arg0) {

        try {

            String data = URLEncoder.encode("namefamily", "UTF8") + "=" + URLEncoder.encode(Namefamily, "UTF8");
            data += "&" + URLEncoder.encode("phnumber", "UTF8") + "=" + URLEncoder.encode(Phnumber, "UTF8");
            data += "&" + URLEncoder.encode("nobimname", "UTF8") + "=" + URLEncoder.encode(Nobimname, "UTF8");
            data += "&" + URLEncoder.encode("discription", "UTF8") + "=" + URLEncoder.encode(Discription, "UTF8");

            URL mylink = new URL(Link);
            URLConnection connect = mylink.openConnection();

            connect.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(connect.getOutputStream());
            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            astalam_bimename.res = sb.toString();
        }

        catch (Exception e) {

        }

        return "";
    }

}
