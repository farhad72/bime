package clas;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import android.os.AsyncTask;
import com.androidbegin.sidemenutabstutorial.sodor_atomobil;


public class clas_davat_b_hamkari extends AsyncTask {

    private String Link         = "";
    private String Name_davat   = "";
    private String Padar_davat  = "";
    private String Tasile_davat = "";
    private String Maghta_davat = "";
    private String Hghogh_davat = "";
    private String Svabgh_davat = "";
    private String Mharat_davat = "";
    private String Phan_davat   = "";
    private String Addrs_davat  = "";
    private String Shaghl       = "";


    public clas_davat_b_hamkari(String link, String name_davat,
                                String padar_davat, String tasile_davat,
                                String maghta_davat, String hghogh_davat,
                                String svabgh_davat, String mharat_davat,
                                String phan_davat, String addrs_davat, String shaghl) {
        Link = link;
        Name_davat = name_davat;
        Padar_davat = padar_davat;
        Tasile_davat = tasile_davat;
        Maghta_davat = maghta_davat;
        Hghogh_davat = hghogh_davat;
        Svabgh_davat = svabgh_davat;
        Mharat_davat = mharat_davat;
        Phan_davat = phan_davat;
        Addrs_davat = addrs_davat;
        Shaghl = shaghl;

    }


    @Override
    protected String doInBackground(Object... arg0) {

        try {

            String data = URLEncoder.encode("name_davat", "UTF8") + "=" + URLEncoder.encode(Name_davat, "UTF8");
            data += "&" + URLEncoder.encode("padar_davat", "UTF8") + "=" + URLEncoder.encode(Padar_davat, "UTF8");
            data += "&" + URLEncoder.encode("tasile_davat", "UTF8") + "=" + URLEncoder.encode(Tasile_davat, "UTF8");
            data += "&" + URLEncoder.encode("maghta_davat", "UTF8") + "=" + URLEncoder.encode(Maghta_davat, "UTF8");
            data += "&" + URLEncoder.encode("hghogh_davat", "UTF8") + "=" + URLEncoder.encode(Hghogh_davat, "UTF8");
            data += "&" + URLEncoder.encode("svabgh_davat", "UTF8") + "=" + URLEncoder.encode(Svabgh_davat, "UTF8");
            data += "&" + URLEncoder.encode("mharat_davat", "UTF8") + "=" + URLEncoder.encode(Mharat_davat, "UTF8");
            data += "&" + URLEncoder.encode("phan_davat", "UTF8") + "=" + URLEncoder.encode(Phan_davat, "UTF8");
            data += "&" + URLEncoder.encode("addrs_davat", "UTF8") + "=" + URLEncoder.encode(Addrs_davat, "UTF8");
            data += "&" + URLEncoder.encode("shaghl", "UTF8") + "=" + URLEncoder.encode(Shaghl, "UTF8");

            URL mylink = new URL(Link);
            URLConnection connect = mylink.openConnection();

            connect.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(connect.getOutputStream());
            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            sodor_atomobil.res = sb.toString();
        }

        catch (Exception e) {

        }

        return "";
    }

}
