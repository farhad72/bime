package clas;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import android.os.AsyncTask;
import com.androidbegin.sidemenutabstutorial.sodor_atomobil;


public class clas_masolyat_sakhtman extends AsyncTask {

    private String Link                  = "";
    private String Metr_sakhtman         = "";
    private String Peshraft_sakhtman     = "";
    private String Time_sakhtman         = "";
    private String Addrs_sakhtman        = "";
    private String Pzesh_sakhtman        = "";
    private String Tedad_nafrat_sakhtman = "";
    private String No_poshsh_sakhtman    = "";
    private String Asklat_sakhtman       = "";


    public clas_masolyat_sakhtman(String link, String metr_sakhtman,
                                  String peshraft_sakhtman, String time_sakhtman,
                                  String addrs_sakhtman, String pzesh_sakhtman,
                                  String tedad_nafrat_sakhtman, String no_poshsh_sakhtman,
                                  String asklat_sakhtman) {
        Link = link;
        Metr_sakhtman = metr_sakhtman;
        Peshraft_sakhtman = peshraft_sakhtman;
        Time_sakhtman = time_sakhtman;
        Addrs_sakhtman = addrs_sakhtman;
        Pzesh_sakhtman = pzesh_sakhtman;
        Tedad_nafrat_sakhtman = tedad_nafrat_sakhtman;
        No_poshsh_sakhtman = no_poshsh_sakhtman;
        Asklat_sakhtman = asklat_sakhtman;

    }


    @Override
    protected String doInBackground(Object... arg0) {

        try {

            String data = URLEncoder.encode("metr_sakhtman", "UTF8") + "=" + URLEncoder.encode(Metr_sakhtman, "UTF8");
            data += "&" + URLEncoder.encode("peshraft_sakhtman", "UTF8") + "=" + URLEncoder.encode(Peshraft_sakhtman, "UTF8");
            data += "&" + URLEncoder.encode("time_sakhtman", "UTF8") + "=" + URLEncoder.encode(Time_sakhtman, "UTF8");
            data += "&" + URLEncoder.encode("addrs_sakhtman", "UTF8") + "=" + URLEncoder.encode(Addrs_sakhtman, "UTF8");
            data += "&" + URLEncoder.encode("pzesh_sakhtman", "UTF8") + "=" + URLEncoder.encode(Pzesh_sakhtman, "UTF8");
            data += "&" + URLEncoder.encode("tedad_nafrat_sakhtman", "UTF8") + "=" + URLEncoder.encode(Tedad_nafrat_sakhtman, "UTF8");
            data += "&" + URLEncoder.encode("no_poshsh_sakhtman", "UTF8") + "=" + URLEncoder.encode(No_poshsh_sakhtman, "UTF8");
            data += "&" + URLEncoder.encode("asklat_sakhtman", "UTF8") + "=" + URLEncoder.encode(Asklat_sakhtman, "UTF8");
            URL mylink = new URL(Link);
            URLConnection connect = mylink.openConnection();

            connect.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(connect.getOutputStream());
            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            sodor_atomobil.res = sb.toString();
        }

        catch (Exception e) {

        }

        return "";
    }

}
