package clas;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import android.os.AsyncTask;
import com.androidbegin.sidemenutabstutorial.mshakhsat_dafater;


public class clas_sabtmshakhsat extends AsyncTask {

    private String Link        = "";
    private String Name        = "";
    private String Namesharkat = "";
    private String Addrss      = "";
    private String Number      = "";
    private String Email       = "";


    public clas_sabtmshakhsat(String link, String name, String namesharkat,
                              String addrss, String number, String email) {
        Link = link;
        Name = name;
        Namesharkat = namesharkat;
        Addrss = addrss;
        Number = number;
        Email = email;

    }


    @Override
    protected String doInBackground(Object... arg0) {

        try {

            String data = URLEncoder.encode("name", "UTF8") + "=" + URLEncoder.encode(Name, "UTF8");
            data += "&" + URLEncoder.encode("namesharkat", "UTF8") + "=" + URLEncoder.encode(Namesharkat, "UTF8");
            data += "&" + URLEncoder.encode("addrss", "UTF8") + "=" + URLEncoder.encode(Addrss, "UTF8");
            data += "&" + URLEncoder.encode("number", "UTF8") + "=" + URLEncoder.encode(Number, "UTF8");
            data += "&" + URLEncoder.encode("email", "UTF8") + "=" + URLEncoder.encode(Email, "UTF8");

            URL mylink = new URL(Link);
            URLConnection connect = mylink.openConnection();

            connect.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(connect.getOutputStream());
            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            mshakhsat_dafater.res = sb.toString();
        }

        catch (Exception e) {

        }

        return "";
    }

}
