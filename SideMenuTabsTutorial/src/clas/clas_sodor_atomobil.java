package clas;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import android.os.AsyncTask;
import com.androidbegin.sidemenutabstutorial.sodor_atomobil;


public class clas_sodor_atomobil extends AsyncTask {

    private String Link                 = "";
    private String Name_atomobil        = "";
    private String Adrass_atomobilz     = "";
    private String Takhfif_atomobil     = "";
    private String Data_anghza_atomobil = "";
    private String No_vasela_atomobil   = "";
    private String Phan_atomobil        = "";


    public clas_sodor_atomobil(String link, String name_atomobil, String adrass_atomobilz,
                               String takhfif_atomobil,
                               String data_anghza_atomobil
                               , String no_vasela_atomobil, String phan_atomobil) {
        Link = link;
        Name_atomobil = name_atomobil;
        Adrass_atomobilz = adrass_atomobilz;
        Takhfif_atomobil = takhfif_atomobil;
        Data_anghza_atomobil = data_anghza_atomobil;
        No_vasela_atomobil = no_vasela_atomobil;
        Phan_atomobil = phan_atomobil;

    }


    @Override
    protected String doInBackground(Object... arg0) {

        try {

            String data = URLEncoder.encode("name_atomobil", "UTF8") + "=" + URLEncoder.encode(Name_atomobil, "UTF8");
            data += "&" + URLEncoder.encode("adrass_atomobilz", "UTF8") + "=" + URLEncoder.encode(Adrass_atomobilz, "UTF8");
            data += "&" + URLEncoder.encode("takhfif_atomobil", "UTF8") + "=" + URLEncoder.encode(Takhfif_atomobil, "UTF8");
            data += "&" + URLEncoder.encode("data_anghza_atomobil", "UTF8") + "=" + URLEncoder.encode(Data_anghza_atomobil, "UTF8");
            data += "&" + URLEncoder.encode("no_vasela_atomobil", "UTF8") + "=" + URLEncoder.encode(No_vasela_atomobil, "UTF8");
            data += "&" + URLEncoder.encode("phan_atomobil", "UTF8") + "=" + URLEncoder.encode(Phan_atomobil, "UTF8");

            URL mylink = new URL(Link);
            URLConnection connect = mylink.openConnection();

            connect.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(connect.getOutputStream());
            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            sodor_atomobil.res = sb.toString();
        }

        catch (Exception e) {

        }

        return "";
    }

}
