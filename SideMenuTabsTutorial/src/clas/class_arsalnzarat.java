package clas;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import android.os.AsyncTask;
import com.androidbegin.sidemenutabstutorial.arsalnzarat;


public class class_arsalnzarat extends AsyncTask {

    private String Link   = "";
    private String Mozo   = "";
    private String Matn   = "";
    private String Status = "";


    public class_arsalnzarat(String link, String mozo, String matn, String status) {
        Link = link;
        Mozo = mozo;
        Matn = matn;
        Status = status;

    }


    @Override
    protected String doInBackground(Object... arg0) {

        try {

            String data = URLEncoder.encode("mozo", "UTF8") + "=" + URLEncoder.encode(Mozo, "UTF8");
            data += "&" + URLEncoder.encode("matn", "UTF8") + "=" + URLEncoder.encode(Matn, "UTF8");
            data += "&" + URLEncoder.encode("status", "UTF8") + "=" + URLEncoder.encode(Status, "UTF8");

            URL mylink = new URL(Link);
            URLConnection connect = mylink.openConnection();

            connect.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(connect.getOutputStream());
            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            arsalnzarat.res = sb.toString();
        }

        catch (Exception e) {

        }

        return "";
    }

}
