package clas_d_p_dolat;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import android.os.AsyncTask;
import com.androidbegin.sidemenutabstutorial.d_p_dolat_news;


public class clas_getcount_new extends AsyncTask {

    private String Link = "";


    public clas_getcount_new(String link) {
        Link = link;

    }


    @Override
    protected String doInBackground(Object... arg0) {

        try {

            URL mylink = new URL(Link);
            URLConnection connect = mylink.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            d_p_dolat_news.countS = sb.toString();
        }

        catch (Exception e) {

        }

        return "";
    }

}
