package clas_p10;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import com.androidbegin.sidemenutabstutorial.P_news;
import android.os.AsyncTask;


public class clas_getcount_p_news extends AsyncTask {

    private String Link = "";


    public clas_getcount_p_news(String link) {
        Link = link;

    }


    @Override
    protected String doInBackground(Object... arg0) {

        try {

            URL mylink = new URL(Link);
            URLConnection connect = mylink.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            P_news.countS = sb.toString();
        }

        catch (Exception e) {

        }

        return "";
    }

}
