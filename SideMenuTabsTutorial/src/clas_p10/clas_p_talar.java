package clas_p10;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import android.content.Context;
import android.os.AsyncTask;

import com.androidbegin.sidemenutabstutorial.database;
import com.androidbegin.sidemenutabstutorial.p_10_talar;


@SuppressWarnings("rawtypes")
public class clas_p_talar extends AsyncTask {

    private String   Link  = "";
    private String   Count = "";

    private String   tid   = "";
    private String   tuser = "";
    private String   tmatn = "";

    private database db;


    public clas_p_talar(String link, String count, Context c) {

        Link = link;
        Count = count;

        db = new database(c);

    }


    @Override
    protected String doInBackground(Object... arg0) {

        try {

            String data = URLEncoder.encode("count", "UTF8") + "=" + URLEncoder.encode(Count, "UTF8");
            URL mylink = new URL(Link);
            URLConnection connect = mylink.openConnection();
            connect.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(connect.getOutputStream());
            wr.write(data);
            wr.flush();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;

            db.open();
            while ((line = reader.readLine()) != null) {

                int f = 0;
                int c = 0;

                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) == '|') {
                        String temp = line.substring(f, i);
                        if (c == 0) {

                            tid = temp;
                        }
                        if (c == 1) {

                            tuser = temp;
                        }
                        if (c == 2) {

                            tmatn = temp.replace("^", "\n");

                        }

                        f = i + 1;
                        c += 1;
                    }
                }
                if ( !tid.equals("")) {

                    db.insert_p_talar(tid, tuser, tmatn);
                }

                sb.append("t");

            }

            db.close();
            p_10_talar.res = sb.toString();

        }
        catch (Exception e) {

        }

        return "";
    }

}
