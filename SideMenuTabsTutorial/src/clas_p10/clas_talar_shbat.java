package clas_p10;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import android.os.AsyncTask;
import com.androidbegin.sidemenutabstutorial.p_10_talar;


public class clas_talar_shbat extends AsyncTask {

    private String Link = "";
    private String Matn = "";


    public clas_talar_shbat(String link, String matn) {
        Link = link;
        Matn = matn;

    }


    @Override
    protected String doInBackground(Object... arg0) {

        try {

            String data = URLEncoder.encode("matn", "UTF8") + "=" + URLEncoder.encode(Matn, "UTF8");

            URL mylink = new URL(Link);
            URLConnection connect = mylink.openConnection();

            connect.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(connect.getOutputStream());
            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            p_10_talar.sec = sb.toString();
        }

        catch (Exception e) {

        }

        return "";
    }

}
