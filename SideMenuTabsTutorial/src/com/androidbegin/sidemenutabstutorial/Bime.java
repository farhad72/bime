package com.androidbegin.sidemenutabstutorial;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Spinner;


public class Bime extends Activity {

    private Spinner spinner1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bime);

        // Intent intent = new Intent(this, MyServic.class);
        // startService(intent);
        ImageView map = (ImageView) findViewById(R.id.imageView1);
        ImageView m_sodor = (ImageView) findViewById(R.id.imageView2);
        ImageView astalam_bimename = (ImageView) findViewById(R.id.imageView3);
        ImageView sodor = (ImageView) findViewById(R.id.imageView4);
        ImageView p_khsart = (ImageView) findViewById(R.id.imageView5);
        ImageView asalt_bimename = (ImageView) findViewById(R.id.imageView6);
        ImageView takhfifat_bime = (ImageView) findViewById(R.id.imageView7);
        ImageView m_sharkat_bime = (ImageView) findViewById(R.id.imageView8);
        ImageView M_bimenamha = (ImageView) findViewById(R.id.imageView9);
        ImageView moghays = (ImageView) findViewById(R.id.imageView10);
        ImageView news = (ImageView) findViewById(R.id.imageView11);
        ImageView ghanon = (ImageView) findViewById(R.id.imageView12);

        ImageView arsalnzarat = (ImageView) findViewById(R.id.imageView15);
        final Typeface font = Typeface.createFromAsset(getAssets(), "font/nazanin.ttf");
        map.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent in = new Intent(Bime.this, map.class);
                startActivity(in);

            }
        });

        m_sodor.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                Intent in = new Intent(Bime.this, M_sodor.class);
                startActivity(in);

            }
        });

        astalam_bimename.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                Intent in = new Intent(Bime.this, astalam_bimename.class);
                startActivity(in);

            }
        });

        sodor.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent in = new Intent(Bime.this, sodor.class);
                startActivity(in);
            }
        });

        p_khsart.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                Intent in = new Intent(Bime.this, p_khsart.class);
                startActivity(in);

            }
        });

        asalt_bimename.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                Intent in = new Intent(Bime.this, asalt_bimename.class);
                startActivity(in);

            }
        });

        takhfifat_bime.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                Intent in = new Intent(Bime.this, takhfifat_bime.class);
                startActivity(in);

            }
        });

        m_sharkat_bime.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                Intent in = new Intent(Bime.this, m_sharkat_bime.class);
                startActivity(in);

            }
        });

        M_bimenamha.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                Intent in = new Intent(Bime.this, M_bimenamha.class);
                startActivity(in);

            }
        });

        news.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent in = new Intent(Bime.this, news.class);
                startActivity(in);

            }
        });

        ghanon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent in = new Intent(Bime.this, ghanon.class);
                startActivity(in);
            }
        });

        arsalnzarat.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent in = new Intent(Bime.this, arsalnzarat.class);
                startActivity(in);

            }
        });

        moghays.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent in = new Intent(Bime.this, moghays.class);
                startActivity(in);

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return true;
    }

}
