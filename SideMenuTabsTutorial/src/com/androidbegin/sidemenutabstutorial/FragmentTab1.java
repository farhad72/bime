package com.androidbegin.sidemenutabstutorial;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.actionbarsherlock.app.SherlockFragment;


public class FragmentTab1 extends SherlockFragment {

    private ImageView         img1;
    private SharedPreferences sp;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragmenttab1, container, false);
        img1 = (ImageView) rootView.findViewById(R.id.imageView1);
        ImageView img2 = (ImageView) rootView.findViewById(R.id.btn_p10);
        final ImageView iv = (ImageView) rootView.findViewById(R.id.image);
        final ImageView iran = (ImageView) rootView.findViewById(R.id.ImageView01);
        final ImageView d_p_dolat = (ImageView) rootView.findViewById(R.id.pish);
        ImageView charter = (ImageView) rootView.findViewById(R.id.charter);
        ImageView maskan = (ImageView) rootView.findViewById(R.id.maskan);
        ImageView azans = (ImageView) rootView.findViewById(R.id.azans);
        ImageView estekhdam = (ImageView) rootView.findViewById(R.id.estekhdam);
        ImageView shoping = (ImageView) rootView.findViewById(R.id.shoping);

        iv.setBackgroundResource(R.anim.shro);
        final AnimationDrawable anim = (AnimationDrawable) iv.getBackground();
        anim.start();
        Thread timer = new Thread() {

            @Override
            public void run() {
                try {
                    sleep(25000000);
                }
                catch (InterruptedException whatIsMyProblem) {
                    whatIsMyProblem.printStackTrace();
                } finally {
                    anim.start();
                }
            }

        };
        timer.start();

        img1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                sp = getActivity().getSharedPreferences("userp", 0);
                if (sp.getInt("status", 0) == 1) {
                    Intent ins = new Intent(getActivity(), Bime.class);

                    startActivity(ins);

                } else {
                    Intent i = new Intent(getActivity(), Bime.class);
                    //   startActivity(i);
                    //Intent ins = new Intent(getActivity(), Login.class);
                    // f();
                    startActivity(i);

                    ;

                }

            }
        });

        img2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(getActivity(), main_page_polic.class);
                startActivity(i);

            }
        });

        iran.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(getActivity(), irankardi_main.class);
                startActivity(i);

            }
        });

        d_p_dolat.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(getActivity(), d_p_dolat.class);
                startActivity(i);

            }
        });

        charter.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Intent i = new Intent(getActivity(), charter.class);
                startActivity(i);
            }
        });
        maskan.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                Intent i = new Intent(getActivity(), maskan.class);
                startActivity(i);

            }
        });
        azans.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent i = new Intent(getActivity(), azans.class);
                startActivity(i);

            }
        });
        estekhdam.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent i = new Intent(getActivity(), davat_b_hamkari.class);
                startActivity(i);

            }
        });
        shoping.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent i = new Intent(getActivity(), shoping.class);
                startActivity(i);

            }
        });

        return rootView;
    }

}