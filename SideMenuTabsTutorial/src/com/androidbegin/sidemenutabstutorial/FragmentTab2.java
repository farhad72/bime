package com.androidbegin.sidemenutabstutorial;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.actionbarsherlock.app.SherlockFragment;


public class FragmentTab2 extends SherlockFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragmenttab2, container, false);
        ImageView kh_b_froshmasolat_amozeshi = (ImageView) rootView.findViewById(R.id.a_kh_bime);

        kh_b_froshmasolat_amozeshi.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(getActivity(), kh_bime.class);
                startActivity(i);

            }
        });

        return rootView;
    }

}
