package com.androidbegin.sidemenutabstutorial;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;
import login.class_login;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;


public class Login extends Activity {

    public static String      res = "";
    private EditText          usertext, passtext;
    private Button            login, exit, reqast;
    private CheckBox          cb;
    private SharedPreferences sp;
    private int               server;
    private String            serialnumber;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        usertext = (EditText) findViewById(R.id.usertext);
        passtext = (EditText) findViewById(R.id.passtext);
        reqast = (Button) findViewById(R.id.reqast);

        login = (Button) findViewById(R.id.login);
        exit = (Button) findViewById(R.id.exit);
        cb = (CheckBox) findViewById(R.id.cb);

        serialnumber = serialnumber();

        reqast.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent in = new Intent(Login.this, reqestpassword.class);
                startActivity(in);

            }
        });

        login.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                new NetCheck().execute();

            }

        });

        exit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                finish();

            }
        });

    }


    @SuppressWarnings("unchecked")
    public void login(final String user, String pass, String serialnumber, final int status) {
        new class_login("http://app.refairan.com/php/login.php", user, pass, serialnumber).execute();

        final ProgressDialog pd = new ProgressDialog(Login.this);
        pd.setMessage("لطفا منتظر بماند تا حساب کاربری شما فعال شود ");
        pd.show();

        final Timer tm = new Timer();
        tm.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        server++;
                        if (server == 20) {
                            pd.cancel();
                            tm.cancel();
                            Toast.makeText(getApplicationContext(),
                                    "ارتیاط شما با اینترنت قطع می باشد", Toast.LENGTH_SHORT).show();

                        }

                        if (res.equals("ok")) {
                            pd.cancel();

                            sp = getApplicationContext().getSharedPreferences("userp", 0);
                            Editor edit = sp.edit();
                            edit.putString("username", user);
                            edit.putInt("status", status);
                            edit.commit();

                            Toast.makeText(getApplicationContext(), res, Toast.LENGTH_SHORT).show();

                            Intent in = new Intent(Login.this, Bime.class);
                            res = "";
                            tm.cancel();
                            f();
                            startActivity(in);
                        } else if (res.equals("no user")) {

                            Toast.makeText(getApplication(), res, Toast.LENGTH_SHORT).show();

                            pd.cancel();
                            Toast.makeText(getApplicationContext(),
                                    "نام کاربر شما اشتباه می باشد لطفا دقت فرمائید ", Toast.LENGTH_SHORT).show();
                            res = "";
                            tm.cancel();
                        } else if (res.equals("password woring")) {

                            pd.cancel();
                            Toast.makeText(getApplicationContext(),
                                    "پسورد شما اشتباه می باشد لطفا دقت فرمائید ", Toast.LENGTH_SHORT).show();

                            res = "";
                            tm.cancel();
                        } else if (res.equals("serial")) {
                            pd.cancel();
                            Toast.makeText(getApplicationContext(),
                                    "این برنامه بر روی گوشی دیگری نصب گردیده و شما اجازه استفاده از آن ندارید" +
                                            " ", Toast.LENGTH_SHORT).show();
                            res = "";
                            tm.cancel();

                        }

                    }

                });

            }
        }, 1, 1000);

    }


    private void f() {

        this.finish();
    }


    public String serialnumber()

    {
        TelephonyManager tManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        final String phone_serial_number = (String) tManager.getDeviceId();
        return phone_serial_number;
    }


    private class NetCheck extends AsyncTask<String, String, Boolean>
    {

        private ProgressDialog nDialog;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nDialog = new ProgressDialog(Login.this);
            nDialog.setTitle("بررسی اتصال اینترنت");
            nDialog.setMessage("چند لحظه صبر کنید....");
            nDialog.setIndeterminate(false);
            nDialog.setCancelable(true);
            nDialog.show();
        }


        /**
         * Gets current device state and checks for working internet connection by trying Google.
         **/
        @Override
        protected Boolean doInBackground(String... args) {

            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()) {
                try {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                    urlc.setConnectTimeout(3000);
                    urlc.connect();
                    if (urlc.getResponseCode() == 200) {
                        return true;
                    }
                }
                catch (MalformedURLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return false;

        }


        @Override
        protected void onPostExecute(Boolean th) {

            if (th == true) {
                nDialog.dismiss();

                if (cb.isChecked())
                {
                    login(usertext.getText().toString(), passtext.getText().toString(), serialnumber, 1);
                }

                else
                {
                    login(usertext.getText().toString(), passtext.getText().toString(), serialnumber, 0);

                }

            }
            else {
                nDialog.dismiss();

                Toast.makeText(getApplication(), "ارتباط شما با اینترنت قطع می باشد ", Toast.LENGTH_SHORT).show();

            }
        }
    }
}