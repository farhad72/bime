package com.androidbegin.sidemenutabstutorial;

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


public class M_bimenamha extends ListActivity {

    private database db;
    private String[] Name;
    private byte[]   p;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.m_bimenamha);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        db = new database(this);
        db.useable();
        refresh();

    }


    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {

        Intent i = new Intent(M_bimenamha.this, show_matn_M_bimenamha.class);
        i.putExtra("ghanon1", Name[position]);

        startActivity(i);
    }


    class AA extends ArrayAdapter<String> {

        public AA() {
            super(M_bimenamha.this, R.layout.row, Name);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater in = getLayoutInflater();
            View row = in.inflate(R.layout.row, parent, false);
            TextView name = (TextView) row.findViewById(R.id.tex_ghanon);
            ImageView img = (ImageView) row.findViewById(R.id.image_ghanon);
            name.setText(Name[position]);
            db.open();
            p = db.getpic("M_bimenamha", position + 1);
            Bitmap pm = BitmapFactory.decodeByteArray(p, 0, p.length);
            img.setImageBitmap(pm);
            db.close();
            return row;
        }
    }


    private void refresh() {

        db.open();
        int s = db.count_inbox("M_bimenamha");
        Name = new String[s];
        for (int i = 0; i < s; i++) {
            Name[i] = db.show_titer("M_bimenamha", i);

        }
        setListAdapter(new AA());
        db.close();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //   Intent intent = new Intent(this, uy.class);
                //   intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //   startActivity(intent);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

}