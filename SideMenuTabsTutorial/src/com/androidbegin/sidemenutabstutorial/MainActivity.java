package com.androidbegin.sidemenutabstutorial;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;


public class MainActivity extends SherlockFragmentActivity {

    // Declare Variable
    DrawerLayout          mDrawerLayout;
    ListView              mDrawerList;
    ActionBarDrawerToggle mDrawerToggle;
    MenuListAdapter       mMenuAdapter;
    String[]              title;
    String[]              subtitle;
    int[]                 icon;
    Fragment              fragment1  = new Fragment1();
    Fragment              fragment2  = new Fragment2();
    private database      db;

    String                file_url   = "http://app.refairan.com/mainapp/updata/SideMenuTabsTutorial.apk";
    ProgressDialog        prgDialog;
    String                Versionurl = "http://app.refairan.com/php/Versionurl.php";
    String                Version    = "1.0";
    Boolean               permision  = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_main);
        db = new database(this);
        db.useable();

        // Generate title
        title = new String[]{ "استفاده کننده خدمات", "تنظیمات", "بروز رسانی", "درباره ما" };

        // Generate subtitle
        subtitle = new String[]{ "", "", "", "" };

        // Generate icon
        icon = new int[]{ R.drawable.action_about, R.drawable.ic_action_setting, R.drawable.ic_action_refresh, R.drawable.ic_action_about };

        // Locate DrawerLayout in drawer_main.xml
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Locate ListView in drawer_main.xml
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // Set a custom shadow that overlays the main content when the drawer
        // opens
        // mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
        //       GravityCompat.START);

        // Pass results to MenuListAdapter Class
        mMenuAdapter = new MenuListAdapter(this, title, subtitle, icon);

        // Set the MenuListAdapter to the ListView
        mDrawerList.setAdapter(mMenuAdapter);

        // Capture button clicks on side menu
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // Enable ActionBar app icon to behave as action to toggle nav drawer

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.action_settings, 0, 0) {

            @Override
            public void onDrawerClosed(View view) {
                // TODO Auto-generated method stub
                super.onDrawerClosed(view);
            }


            @Override
            public void onDrawerOpened(View drawerView) {
                // TODO Auto-generated method stub
                super.onDrawerOpened(drawerView);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {

            selectItem(0);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        item.getItemId();

        switch (item.getItemId())
        {
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }
                break;

            case R.id.open:
                new check().execute(Versionurl);
                break;

            case R.id.setting:
                Intent in = new Intent(MainActivity.this, font.class);
                startActivity(in);
                break;
            default:
                break;

        }

        return super.onOptionsItemSelected(item);
    }


    // The click listener for ListView in the navigation drawer
    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            selectItem(position);
        }
    }


    private void selectItem(int position) {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // Locate Position
        switch (position) {
            case 0:

                // Intent in = new Intent(MainActivity.this, font.class);
                // startActivity(in);
                ft.replace(R.id.content_frame, fragment1);
                break;
            case 1:
                // ft.replace(R.id.content_frame, fragment2);
                Intent in = new Intent(MainActivity.this, font.class);
                startActivity(in);

                break;
            case 2:
                new check().execute(Versionurl);
                break;
            case 3:
                setContentView(R.layout.about);
                break;

        }
        ft.commit();
        mDrawerList.setItemChecked(position, true);
        // Close drawer
        mDrawerLayout.closeDrawer(mDrawerList);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    ///////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
    ////////////*******************************

    @Override
    protected Dialog onCreateDialog(int id)
    {
        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage("در حال به روز رسانی نرم افزار \nلطفا کمی صبر کنید");
        prgDialog.setMax(100);
        prgDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        prgDialog.show();
        return prgDialog;
    }


    private class UpdateApp extends AsyncTask<String, String, Void>
    {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            showDialog(0);
        }


        @Override
        protected Void doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);

                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.connect();
                String PATH = "/mnt/sdcard/download/";
                File file = new File(PATH);
                file.mkdir();

                String AppName = getString(R.string.app_name) + ".apk";
                File outputfile = new File(file, AppName);
                if (outputfile.exists())
                    outputfile.delete();
                FileOutputStream fos = new FileOutputStream(outputfile);
                InputStream is = c.getInputStream();
                int lenght = c.getContentLength();
                byte[] buffer = new byte[1024];
                int len;
                int total = 0;
                while ((len = is.read(buffer)) != -1)
                {
                    total = total + len;
                    fos.write(buffer, 0, len);
                    publishProgress("" + (int) ((total * 100) / lenght));
                }
                fos.flush();
                fos.close();
                is.close();

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/download/" + AppName)),
                        "application/vnd.android.package-archive");

                startActivity(intent);

            }
            catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onProgressUpdate(String... values)
        {
            prgDialog.setProgress(Integer.parseInt(values[0]));
        }


        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            dismissDialog(0);
        }

    }


    private class check extends AsyncTask<String, Void, String>
    {

        private ProgressDialog Dialog;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Dialog = new ProgressDialog(MainActivity.this);
            Dialog.setMessage("در حال ارتباط با سرور");
            Dialog.show();

        }


        @Override
        protected String doInBackground(String... params) {
            try
            {
                String url = params[0];
                HttpClient httpClient = new DefaultHttpClient();
                HttpContext localContext = new BasicHttpContext();
                HttpGet httpGet = new HttpGet(url);
                HttpResponse httpResponse = httpClient.execute
                        (httpGet, localContext);

                BufferedReader reader = new BufferedReader
                        (new InputStreamReader(httpResponse.getEntity().
                                getContent()));

                char line[];
                line = reader.readLine().toCharArray();
                String a = new StringBuilder().append(line[0]).append(line[1])
                        .append(line[2]).toString();

                if (Version.equals(a))
                    permision = false;
                else
                    permision = true;

            }
            catch (ClientProtocolException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            Dialog.dismiss();

            if (permision == true)
                new UpdateApp().execute(file_url);
            else
                Toast.makeText(MainActivity.this, "برنامه به روز است", Toast.LENGTH_SHORT).show();
        }

    }
}
