package com.androidbegin.sidemenutabstutorial;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import android.R.integer;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.widget.Toast;


public class MyServic extends Service {

    private static final int NOT_ID = 1;
    private database         db;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        db = new database(this);
        db.useable();

        new NetCheck().execute();

        return Service.START_FLAG_REDELIVERY;
    }


    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }


    private class DoBackgroundTask extends AsyncTask<URL, integer, String> {

        private String Link = "";

        private String countS;


        public DoBackgroundTask(String link) {
            Link = link;

        }


        @Override
        protected String doInBackground(URL... arg0) {

            try {

                URL mylink = new URL(Link);
                URLConnection connect = mylink.openConnection();
                BufferedReader reader = new BufferedReader(new InputStreamReader(connect.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                countS = sb.toString();

            }

            catch (Exception e) {

            }

            return countS;
        }


        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            db.open();
            int countL = db.count_inbox("takhfifat_bime");
            db.close();
            int b = Integer.parseInt(result) - countL;
            //  Toast.makeText(getBaseContext(), "" + b, Toast.LENGTH_LONG).show();
            if (b > 0)
            {
                String ns = Context.NOTIFICATION_SERVICE;
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);

                int icon = R.drawable.ic_action_unread;
                CharSequence tickerText = " بیمه سامان ";
                long when = System.currentTimeMillis();

                int requestID = (int) System.currentTimeMillis();
                Notification notification = new Notification(icon, tickerText, when);
                Context context = getApplicationContext();
                Intent notificationIntent = new Intent(context, takhfifat_bime.class);
                //notificationIntent.putExtra("data1", "My Data 1");
                //  notificationIntent.putExtra("data2", "My Data 2");
                //  notificationIntent.setAction("myString" + requestID);
                PendingIntent contentIntent = PendingIntent.getActivity(context, requestID, notificationIntent, 0);
                notificationIntent.setData((Uri.parse("mystring" + requestID)));
                notification.setLatestEventInfo(context, "تخفیفات بیمه ", requestID + "", contentIntent);
                notification.flags += Notification.FLAG_ONGOING_EVENT;
                notification.flags += Notification.FLAG_AUTO_CANCEL;
                mNotificationManager.notify(NOT_ID, notification);
            }

        }

    }


    private class NetCheck extends AsyncTask<String, String, Boolean>
    {

        /**
         * Gets current device state and checks for working internet connection by trying Google.
         **/
        @Override
        protected Boolean doInBackground(String... args) {

            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()) {
                try {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                    urlc.setConnectTimeout(3000);
                    urlc.connect();
                    if (urlc.getResponseCode() == 200) {
                        return true;
                    }
                }
                catch (MalformedURLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return false;

        }


        @Override
        protected void onPostExecute(Boolean th) {

            if (th == true) {

                new DoBackgroundTask("http://app.refairan.com/php/bime/getcount_takhfifat_bime.php").execute();
            }
            else {
                Toast.makeText(getBaseContext(), "inter net", Toast.LENGTH_LONG).show();

            }
        }
    }
}
