package com.androidbegin.sidemenutabstutorial;

import java.util.Timer;
import java.util.TimerTask;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import clas_p10.clas_getcount_p_news;
import clas_p10.clas_p_news;


@SuppressWarnings("unused")
public class P_news extends ListActivity {

    private database     db;
    private String[]     Name;
    private byte[]       p;

    public static String res    = "";

    public static String newskh = "";
    public static String countS = "";
    private int          server = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_news);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        db = new database(this);
        db.useable();
        getcount();
        refresh();

    }


    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {

        Intent i = new Intent(P_news.this, show_matn_news_p.class);
        i.putExtra("mozo", Name[position]);

        startActivity(i);
    }


    class AA extends ArrayAdapter<String> {

        public AA() {
            super(P_news.this, R.layout.row, Name);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater in = getLayoutInflater();
            View row = in.inflate(R.layout.row, parent, false);
            TextView name = (TextView) row.findViewById(R.id.tex_ghanon);
            ImageView img = (ImageView) row.findViewById(R.id.image_ghanon);
            name.setText(Name[position]);

            img.setImageResource(R.drawable.ic_action_back);
            //  db.open();
            //   p = db.getpic("takhfifat_bime", position + 1);
            // Bitmap pm = BitmapFactory.decodeByteArray(p, 0, p.length);
            //  img.setImageBitmap(pm);
            //  db.close();
            return row;
        }
    }


    private void refresh() {

        db.open();
        int s = db.count_inbox("P_news");

        Name = new String[s];
        for (int i = 0; i < s; i++) {
            Name[i] = db.show_titer("P_news", i);

        }
        setListAdapter(new AA());
        db.close();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //   Intent intent = new Intent(this, uy.class);
                //   intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //   startActivity(intent);
                break;

        }
        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("unchecked")
    private void update(String count) {
        new clas_p_news("http://app.refairan.com/php/p+10/p_news.php", count, this).execute();
        final ProgressDialog pd = new ProgressDialog(P_news.this);
        pd.setMessage("لطفا منتظر بماند");
        pd.show();
        final Timer tm = new Timer();
        tm.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        server++;
                        if (server == 5) {
                            pd.cancel();
                            tm.cancel();
                            Toast.makeText(getApplicationContext(), "ارتیاط شما با اینترنت قطع می باشد", Toast.LENGTH_SHORT).show();
                            //  txt.setText("0");

                        }

                        if ( !res.equals("")) {
                            pd.cancel();
                            Toast.makeText(getApplicationContext(), "درحال دریافت جملات", Toast.LENGTH_SHORT).show();
                            //   getcount();
                            tm.cancel();
                            refresh();
                        }
                    }

                });

            }
        }, 1, 1000);
    }


    @SuppressWarnings("unchecked")
    private void getcount() {
        new clas_getcount_p_news("http://app.refairan.com/php/p+10/getcount_news_p.php").execute();

        final ProgressDialog pd = new ProgressDialog(P_news.this);
        pd.setMessage("لطفا منتظر بماند  ");
        pd.show();
        final Timer tm = new Timer();
        tm.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        server++;
                        if (server == 20) {
                            pd.cancel();
                            tm.cancel();
                            Toast.makeText(getApplicationContext(), "لطفا دوباره امتحان کنید ", Toast.LENGTH_SHORT).show();
                            // txt.setText("0");

                        }

                        if ( !countS.equals("")) {
                            pd.cancel();
                            db.open();
                            int countL = db.count_inbox("P_news");
                            db.close();
                            int s = Integer.parseInt(countS) - countL;
                            //  txt.setText(s + "");
                            // txt1.setText(newskh);

                            countS = "";
                            newskh = "";
                            tm.cancel();
                            if (s > 0) {
                                String str = String.valueOf(s);
                                update(str);
                            } else {

                                Toast.makeText(getApplicationContext(), "خبر جدید نیست", Toast.LENGTH_LONG).show();
                                refresh();
                            }

                        }
                    }

                });

            }
        }, 1, 1000);
    }
}