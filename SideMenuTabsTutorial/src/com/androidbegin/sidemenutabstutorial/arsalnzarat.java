package com.androidbegin.sidemenutabstutorial;

import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import clas.class_arsalnzarat;


public class arsalnzarat extends Activity {

    public static String res    = "";
    private int          server = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.arsalnzarat);
        Button btn_nazar = (Button) findViewById(R.id.btn_nazar);
        final EditText mozo = (EditText) findViewById(R.id.edit_mozo);
        final EditText matn = (EditText) findViewById(R.id.edit_matn);

        btn_nazar.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("unchecked")
            @Override
            public void onClick(View arg0) {

                if (find(matn.getText().toString())) {
                    AlertDialog.Builder b = new AlertDialog.Builder(arsalnzarat.this);
                    b.setMessage("شما از کاراکتر غیر مجاز |و^ استفاده کرده ایده");
                    b.setTitle("پیغام خطا");
                    b.setCancelable(false);
                    b.setNegativeButton("حذف کاراکترهای  توسط سیستم", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            matn.setText(matn.getText().toString().replace("|", ""));
                            matn.setText(matn.getText().toString().replace("^", ""));

                        }

                    });

                    b.setNeutralButton("حذف کاراکترهای توسط کاربر", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                        }
                    });
                    AlertDialog alert = b.create();
                    alert.show();

                } else {

                    if (mozo.getText().toString().equals("") || matn.getText().toString().equals(""))
                    {

                        Toast.makeText(getApplicationContext(), "لطفا موضوع و نظر خود را وارد کنید", Toast.LENGTH_SHORT).show();
                    } else
                    {
                        new class_arsalnzarat("http://app.refairan.com/php/bime/arsalnzarat.php", mozo.getText().toString(),
                                matn.getText().toString(), "d").execute();

                        final ProgressDialog pd = new ProgressDialog(arsalnzarat.this);
                        pd.setMessage("در حال ارسال نظر شما  ");
                        pd.show();

                        final Timer tm = new Timer();
                        tm.scheduleAtFixedRate(new TimerTask() {

                            @Override
                            public void run() {

                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {

                                        server++;
                                        if (server == 10) {
                                            pd.cancel();
                                            tm.cancel();
                                            Toast.makeText(getApplicationContext(), "ارتیاط شما با اینترنت قطع می باشد", Toast.LENGTH_SHORT).show();

                                        }

                                        if (res.equals("ok")) {

                                            pd.cancel();
                                            Toast.makeText(getApplicationContext(), "نظر شما با موافقیت ثبت گردیده", Toast.LENGTH_SHORT).show();
                                            res = "";
                                            finish();
                                            tm.cancel();

                                        } else if (res.equals("no")) {

                                            pd.cancel();
                                            Toast.makeText(getApplicationContext(), "خطا لطفا دوبار تلاش کنید", Toast.LENGTH_SHORT).show();

                                            finish();
                                            tm.cancel();

                                        }
                                    }

                                });

                            }
                        }, 1, 1000);
                    }

                }
            }
        });

    }


    public boolean find(String t) {

        for (int i = 0; i < t.length(); i++) {
            if (t.charAt(i) == '|' || t.charAt(i) == '^') {
                return true;
            }

        }
        return false;

    }
}
