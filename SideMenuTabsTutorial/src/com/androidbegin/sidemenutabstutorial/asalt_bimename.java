package com.androidbegin.sidemenutabstutorial;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;


public class asalt_bimename extends Activity {

    private WebView webview;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asalt_bimename);
        webview = (WebView) findViewById(R.id.webView1);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.loadUrl("http://www.centinsur.ir/ShowPage.aspx?page_=form&order=show&lang=1&sub=0&PageId=889");
    }


    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.back, menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {

        item.getItemId();
        switch (item.getItemId()) {
            case R.id.back:
                finish();
                break;
            default:
                break;

        }

        return super.onOptionsItemSelected(item);
    }
}
