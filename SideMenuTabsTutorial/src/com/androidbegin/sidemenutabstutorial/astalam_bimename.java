package com.androidbegin.sidemenutabstutorial;

import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import clas.clas_astalam_bimename;


public class astalam_bimename extends Activity {

    public static String res    = "";
    private int          server = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.astalam_bimename);
        ImageView tamas = (ImageView) findViewById(R.id.tamas);
        ImageView whatsapp = (ImageView) findViewById(R.id.whatsapp);
        ImageView arsal_information = (ImageView) findViewById(R.id.arsal_information);
        final EditText name_family = (EditText) findViewById(R.id.name_family);
        final EditText ph_number = (EditText) findViewById(R.id.ph_number);
        final EditText no_bimname = (EditText) findViewById(R.id.no_bimname);
        final EditText discription = (EditText) findViewById(R.id.discription);

        tamas.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                String number = "tel:" + "09167394338";
                Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
                startActivity(callIntent);

            }
        });

        whatsapp.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                openWhatsappContact("9359391847");

            }
        });
        arsal_information.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("unchecked")
            @Override
            public void onClick(View arg0) {
                if (find(discription.getText().toString())) {
                    AlertDialog.Builder b = new AlertDialog.Builder(astalam_bimename.this);
                    b.setMessage("شما از کاراکتر غیر مجاز |و^ استفاده کرده ایده");
                    b.setTitle("پیغام خطا");
                    b.setCancelable(false);
                    b.setNegativeButton("حذف کاراکترهای  توسط سیستم", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            discription.setText(discription.getText().toString().replace("|", ""));
                            discription.setText(discription.getText().toString().replace("^", ""));

                        }

                    });

                    b.setNeutralButton("حذف کاراکترهای توسط کاربر", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                        }
                    });
                    AlertDialog alert = b.create();
                    alert.show();

                } else {

                    if (name_family.getText().toString().equals("") || ph_number.getText().toString().equals("") ||
                            no_bimname.getText().toString().equals("") || discription.getText().toString().equals(""))
                    {

                        Toast.makeText(getApplicationContext(), "لطفا مقدار را وارد کنید", Toast.LENGTH_SHORT).show();
                    } else
                    {
                        new clas_astalam_bimename("http://app.refairan.com/php/bime/astalam_bimename.php", name_family.getText().toString(),
                                ph_number.getText().toString(), no_bimname.getText().toString(), discription.getText().toString()).execute();

                        final ProgressDialog pd = new ProgressDialog(astalam_bimename.this);
                        pd.setMessage("در حال ارسال اطلاعات شما  ");
                        pd.show();

                        final Timer tm = new Timer();
                        tm.scheduleAtFixedRate(new TimerTask() {

                            @Override
                            public void run() {

                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {

                                        server++;
                                        if (server == 10) {
                                            pd.cancel();
                                            tm.cancel();
                                            Toast.makeText(getApplicationContext(), "ارتیاط شما با اینترنت قطع می باشد", Toast.LENGTH_SHORT).show();

                                        }

                                        if (res.equals("ok")) {

                                            pd.cancel();
                                            Toast.makeText(getApplicationContext(), "اطلاعات شما با موافقیت ثبت گردیده", Toast.LENGTH_SHORT).show();
                                            res = "";
                                            finish();
                                            tm.cancel();

                                        } else if (res.equals("no")) {

                                            pd.cancel();
                                            Toast.makeText(getApplicationContext(), "خطا لطفا دوبار تلاش کنید", Toast.LENGTH_SHORT).show();

                                            finish();
                                            tm.cancel();

                                        }
                                    }

                                });

                            }
                        }, 1, 1000);
                    }

                }
            }
        });

    }


    public void openWhatsappContact(String number) {
        Uri uri = Uri.parse("smsto:" + number);
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.setPackage("com.whatsapp");
        startActivity(Intent.createChooser(i, ""));
    }


    public boolean find(String t) {

        for (int i = 0; i < t.length(); i++) {
            if (t.charAt(i) == '|' || t.charAt(i) == '^') {
                return true;
            }

        }
        return false;

    }

}
