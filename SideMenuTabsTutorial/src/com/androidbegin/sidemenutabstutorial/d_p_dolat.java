package com.androidbegin.sidemenutabstutorial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;


public class d_p_dolat extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.d_p_dolat);

        ImageView news = (ImageView) findViewById(R.id.d_p_dolat_news);
        ImageView d_p_dolat_arakhdamat = (ImageView) findViewById(R.id.d_p_dolat_arakhdamat);
        ImageView map = (ImageView) findViewById(R.id.P_imag1);
        map.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent in = new Intent(d_p_dolat.this, map.class);
                startActivity(in);
            }
        });

        news.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent in = new Intent(d_p_dolat.this, d_p_dolat_news.class);
                startActivity(in);

            }
        });

        d_p_dolat_arakhdamat.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent in = new Intent(d_p_dolat.this, d_p_dolat_arakhdamat.class);
                startActivity(in);

            }
        });
    }

}
