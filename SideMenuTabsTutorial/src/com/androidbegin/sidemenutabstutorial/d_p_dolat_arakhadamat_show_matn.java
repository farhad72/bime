package com.androidbegin.sidemenutabstutorial;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;


public class d_p_dolat_arakhadamat_show_matn extends Activity {

    private String       mozo;

    private database     db;
    private String       s;
    private LinearLayout ll;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.d_p_dolat_arakhadamat_show_matn);
        db = new database(this);
        db.useable();

        ll = (LinearLayout) findViewById(R.id.myll);
        //  String text = "kavos";

        TextView text_mozo = (TextView) findViewById(R.id.text_titer_M_bimenamha);
        // TextView text_matn = (TextView) findViewById(R.id.text_matn_M_bimenamha);
        Bundle ex = getIntent().getExtras();
        mozo = ex.getString("ghanon1");
        db.open();
        s = db.show_matn("d_p_dolat_arakhadamat", mozo);
        db.close();
        //  matn = ex.getString("matn");
        text_mozo.setText(mozo);
        //  text_matn.setText(s);
        ctext(s);

    }


    private void ctext(String text) {

        TextView tv = new TextView(d_p_dolat_arakhadamat_show_matn.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp.setMargins(0, 10, 0, 10);
        tv.setPadding(10, 10, 10, 10);
        lp.gravity = Gravity.CENTER;
        tv.setText(text);
        ll.addView(tv, lp);

    }
}
