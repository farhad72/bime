package com.androidbegin.sidemenutabstutorial;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class database extends SQLiteOpenHelper {

    public final String   path = "data/data/com.androidbegin.sidemenutabstutorial/databases/";
    public final String   Name = "database";
    public SQLiteDatabase mydb;

    private final Context mycontext;


    public database(Context context) {
        super(context, "database", null, 1);
        mycontext = context;

    }


    @Override
    public void onCreate(SQLiteDatabase arg0) {

    }


    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

    }


    public void useable() {

        boolean checkdb = checkdb();

        if (checkdb) {

        } else {

            this.getReadableDatabase();

            try {
                copydatabase();
            }
            catch (IOException e) {

            }

        }

    }


    public void open() {

        mydb = SQLiteDatabase.openDatabase(path + Name, null, SQLiteDatabase.OPEN_READWRITE);

    }


    @Override
    public void close() {
        mydb.close();
    }


    public boolean checkdb() {

        SQLiteDatabase db = null;
        try {
            db = SQLiteDatabase.openDatabase(path + Name, null, SQLiteDatabase.OPEN_READONLY);
        }
        catch (SQLException e)
        {

        }
        return db != null ? true : false;

    }


    public void copydatabase() throws IOException {
        OutputStream myOutput = new FileOutputStream(path + Name);
        byte[] buffer = new byte[1024];
        int length;
        InputStream myInput = mycontext.getAssets().open(Name);
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        myInput.close();
        myOutput.flush();
        myOutput.close();
    }


    /////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////
    public Integer count_inbox(String table) {
        Cursor cu = mydb.query(table, null, null, null, null, null, null);
        int s = cu.getCount();
        return s;
    }


    public String show_titer(String table, int row) {

        Cursor cu = mydb.rawQuery("select * from " + table, null);
        cu.moveToPosition(row);
        String s = cu.getString(2);
        return s;
    }


    public String show_matn(String table, String sea) {

        Cursor cu = mydb.rawQuery("select * from " + table + " where titer='" + sea + "'", null);
        cu.moveToFirst();
        String s = cu.getString(3);
        return s;
    }


    public byte[] getpic(String table, int i) {
        Cursor cu = mydb.rawQuery("select * from " + table + " where ID='" + i + "'", null);
        cu.moveToFirst();
        byte[] s;
        s = cu.getBlob(4);
        return s;

    }


    public String amozesh(String table, int mozo) {

        Cursor cu = mydb.rawQuery("select * from " + table + " where ID='" + mozo + "'", null);
        cu.moveToFirst();
        String s = cu.getString(1);
        return s;
    }


    public String moghays(String table, String sea, int fild) {

        Cursor cu = mydb.rawQuery("select * from " + table + " where name='" + sea + "'", null);
        cu.moveToFirst();
        String s = cu.getString(fild);
        return s;
    }


    public void insert(String tid, String tuser, String tmatn) {
        ContentValues cv = new ContentValues();
        cv.put("idt", tid);
        cv.put("titer", tuser);
        cv.put("matn", tmatn);
        mydb.insert("takhfifat_bime", "idt", cv);

    }


    public void insert_news(String tid, String tuser, String tmatn) {
        ContentValues cv = new ContentValues();
        cv.put("idt", tid);
        cv.put("titer", tuser);
        cv.put("matn", tmatn);
        mydb.insert("news", "idt", cv);

    }


    public void insert_news_p(String tid, String tuser, String tmatn) {
        ContentValues cv = new ContentValues();
        cv.put("idt", tid);
        cv.put("titer", tuser);
        cv.put("matn", tmatn);
        mydb.insert("P_news", "idt", cv);

    }


    public void insert_p_talar(String tid, String tuser, String tmatn) {
        ContentValues cv = new ContentValues();
        cv.put("idt", tid);
        cv.put("titer", tuser);
        cv.put("matn", tmatn);
        mydb.insert("whatsapp", "idt", cv);

    }


    public void insert_d_p_dolat_news(String tid, String tuser, String tmatn) {
        ContentValues cv = new ContentValues();
        cv.put("idt", tid);
        cv.put("titer", tuser);
        cv.put("matn", tmatn);
        mydb.insert("d_p_dolat_news", "idt", cv);

    }


    public String TALAR(String table, int row) {

        Cursor cu = mydb.rawQuery("select * from " + table, null);
        cu.moveToPosition(row);
        String s = cu.getString(3);
        return s;
    }


    public Integer count_iran(String table, String field) {

        Cursor cu = mydb.rawQuery("select * from " + table + " group by " + field, null);
        int s = cu.getCount();
        return s;
    }


    public String iran_display(String table, int row) {

        Cursor cu = mydb.rawQuery("select * from " + table + " group by a", null);
        cu.moveToPosition(row);
        String s = cu.getString(2);
        return s;
    }


    public String a(String table, int row) {

        Cursor cu = mydb.rawQuery("select * from " + table + " group by a", null);
        cu.moveToPosition(row);
        String s = cu.getString(1);
        return s;
    }


    public byte[] getpic_iran(String table, int i) {
        Cursor cu = mydb.rawQuery("select * from " + table + " where ID='" + i + "'", null);
        cu.moveToFirst();
        byte[] s;
        s = cu.getBlob(5);
        return s;

    }


    public Integer iran_page_count(String table, String sea) {

        Cursor cu = mydb.rawQuery("select * from " + table + " where a='" + sea + "'", null);
        int s = cu.getCount();
        return s;
    }


    public String iran_main_display(String table, String sea, String page) {

        Cursor cu = mydb.rawQuery("select * from " + table + " where a='" + sea + "' and Page=" + page, null);
        cu.moveToFirst();
        String s = cu.getString(3);
        return s;
    }

}
