package com.androidbegin.sidemenutabstutorial;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class database1 extends SQLiteOpenHelper {

    public final String   path = "data/data/com.androidbegin.sidemenutabstutorial/databases/";
    public final String   Name = "database1";
    public SQLiteDatabase mydb;

    private final Context mycontext;


    public database1(Context context) {
        super(context, "database1", null, 1);
        mycontext = context;

    }


    @Override
    public void onCreate(SQLiteDatabase arg0) {

    }


    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

    }


    public void useable() {

        boolean checkdb = checkdb();

        if (checkdb) {

        } else {

            this.getReadableDatabase();

            try {
                copydatabase();
            }
            catch (IOException e) {

            }

        }

    }


    public void open() {

        mydb = SQLiteDatabase.openDatabase(path + Name, null, SQLiteDatabase.OPEN_READWRITE);

    }


    @Override
    public void close() {
        mydb.close();
    }


    public boolean checkdb() {

        SQLiteDatabase db = null;
        try {
            db = SQLiteDatabase.openDatabase(path + Name, null, SQLiteDatabase.OPEN_READONLY);
        }
        catch (SQLException e)
        {

        }
        return db != null ? true : false;

    }


    public void copydatabase() throws IOException {
        OutputStream myOutput = new FileOutputStream(path + Name);
        byte[] buffer = new byte[1024];
        int length;
        InputStream myInput = mycontext.getAssets().open(Name);
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        myInput.close();
        myOutput.flush();
        myOutput.close();
    }


    public String Season_display(String table, int row) {

        Cursor cu = mydb.rawQuery("select * from " + table + " group by Seasone", null);
        cu.moveToPosition(row);
        String s = cu.getString(4);
        return s;
    }


    public Integer count(String table, String field) {

        Cursor cu = mydb.rawQuery("select * from " + table + " group by " + field, null);
        int s = cu.getCount();
        return s;
    }


    public Integer Story_count(String table, String sea) {

        Cursor cu = mydb.rawQuery("select * from " + table + " where Seasone='" + sea + "' group by Name", null);
        int s = cu.getCount();
        return s;
    }


    public String Story_display(String table, int row, String sea) {

        Cursor cu = mydb.rawQuery("select * from " + table + " where Seasone='" + sea + "' group by Name", null);
        cu.moveToPosition(row);
        String s = cu.getString(1);
        return s;
    }


    public Integer Story_page_count(String table, String sea, String story) {

        Cursor cu = mydb.rawQuery("select * from " + table + " where Seasone='" + sea + "' and Name='" + story + "'", null);
        int s = cu.getCount();
        return s;
    }


    public String main_display(String table, String sea, String name, String page) {

        Cursor cu = mydb.rawQuery("select * from " + table + " where Seasone='" + sea + "' and Name='" + name + "' and Page=" + page, null);
        cu.moveToFirst();
        String s = cu.getString(2);
        return s;
    }

}
