package com.androidbegin.sidemenutabstutorial;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import clas.clas_davat_b_hamkari;


public class davat_b_hamkari extends Activity {

    public static String res    = "";
    private int          server = 0;
    private Spinner      shaghl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_davat_b_hamkari);
        final EditText name_davat = (EditText) findViewById(R.id.name_davat);
        final EditText padar_davat = (EditText) findViewById(R.id.padar_davat);
        final EditText tasile_davat = (EditText) findViewById(R.id.tasile_davat);
        final EditText maghta_davat = (EditText) findViewById(R.id.maghta_davat);

        final EditText hghogh_davat = (EditText) findViewById(R.id.hghogh_davat);

        final EditText svabgh_davat = (EditText) findViewById(R.id.svabgh_davat);

        final EditText mharat_davat = (EditText) findViewById(R.id.mharat_davat);
        final EditText phan_davat = (EditText) findViewById(R.id.phan_davat);
        final EditText addrs_davat = (EditText) findViewById(R.id.addrs_davat);

        final Button btn_davat = (Button) findViewById(R.id.btn_davat);
        shaghl();

        btn_davat.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("unchecked")
            @Override
            public void onClick(View arg0) {

                Object s = shaghl.getSelectedItem();
                //Object s2 = spinner2.getSelectedItem();
                String shaghl = (String) s;

                if (name_davat.getText().toString().equals("") ||
                        padar_davat.getText().toString().equals("") ||
                        tasile_davat.getText().toString().equals("") ||
                        maghta_davat.getText().toString().equals("") ||

                        hghogh_davat.getText().toString().equals("") ||

                        svabgh_davat.getText().toString().equals("") ||

                        mharat_davat.getText().toString().equals("") ||
                        phan_davat.getText().toString().equals("") ||
                        addrs_davat.getText().toString().equals(""))
                {

                    Toast.makeText(getApplicationContext(), "لطفا مقدار را وارد کنید", Toast.LENGTH_SHORT).show();
                }

                else {

                    new clas_davat_b_hamkari("http://app.refairan.com/php/bime/sodor_atomobil.php",
                            name_davat.getText().toString(),
                            padar_davat.getText().toString(),
                            tasile_davat.getText().toString(),
                            maghta_davat.getText().toString(),

                            hghogh_davat.getText().toString(),

                            svabgh_davat.getText().toString(),

                            mharat_davat.getText().toString(),
                            phan_davat.getText().toString(),
                            addrs_davat.getText().toString(), shaghl).execute();

                    final ProgressDialog pd = new ProgressDialog(davat_b_hamkari.this);
                    pd.setMessage("در حال ارسال اطلاعات شما  ");
                    pd.show();

                    final Timer tm = new Timer();
                    tm.scheduleAtFixedRate(new TimerTask() {

                        @Override
                        public void run() {

                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {

                                    server++;
                                    if (server == 10) {
                                        pd.cancel();
                                        tm.cancel();
                                        Toast.makeText(getApplicationContext(), "ارتیاط شما با اینترنت قطع می باشد", Toast.LENGTH_SHORT).show();

                                    }

                                    if (res.equals("ok")) {

                                        pd.cancel();
                                        Toast.makeText(getApplicationContext(), "اطلاعات شما با موافقیت ثبت گردیده", Toast.LENGTH_SHORT).show();
                                        res = "";
                                        finish();
                                        tm.cancel();

                                    } else if (res.equals("no")) {

                                        pd.cancel();
                                        Toast.makeText(getApplicationContext(), "خطا لطفا دوبار تلاش کنید", Toast.LENGTH_SHORT).show();

                                        finish();
                                        tm.cancel();

                                    }
                                }

                            });

                        }
                    }, 1, 1000);
                }

            }

        });

    }


    public void openWhatsappContact(String number) {
        Uri uri = Uri.parse("smsto:" + number);
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.setPackage("com.whatsapp");
        startActivity(Intent.createChooser(i, ""));
    }


    public boolean find(String t) {

        for (int i = 0; i < t.length(); i++) {
            if (t.charAt(i) == '|' || t.charAt(i) == '^') {
                return true;
            }

        }
        return false;

    }


    @SuppressWarnings("unchecked")
    public void shaghl() {
        shaghl = (Spinner) findViewById(R.id.shaghl);
        List list = new ArrayList();
        list.add("کدام مکان را برای شغل خود انتخاب می کنید");
        list.add("دفاتر بیمه");
        list.add("دفاتر خدمات پستی");
        list.add("بازاریابی");
        list.add("دفاتر پیشخوان دولت");
        list.add("دفاتر پلیس+10");

        ArrayAdapter dataAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        shaghl.setAdapter(dataAdapter);

    }

}
