package com.androidbegin.sidemenutabstutorial;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;


public class font extends Activity {

    private RadioButton       rbsystem, rbnazanin, rbhoma, rbkoodak, rbday, rbnight;
    private TextView          test, s1, s2, s3, s4;
    private SeekBar           sbsize, sbspace;
    private ImageView         save, cancel;

    private Typeface          font, nazanin, homa, koodak;

    private String            sfont;

    private String            mode;

    private SharedPreferences sp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.font);

        set();
        load();

        rbnazanin.setTypeface(Typeface.createFromAsset(getAssets(), "font/nazanin.ttf"));
        rbhoma.setTypeface(Typeface.createFromAsset(getAssets(), "font/homa.ttf"));
        rbkoodak.setTypeface(Typeface.createFromAsset(getAssets(), "font/koodak.ttf"));

        s1.setTypeface(Typeface.createFromAsset(getAssets(), "font/homa.ttf"));
        s2.setTypeface(Typeface.createFromAsset(getAssets(), "font/homa.ttf"));
        s3.setTypeface(Typeface.createFromAsset(getAssets(), "font/homa.ttf"));
        s4.setTypeface(Typeface.createFromAsset(getAssets(), "font/homa.ttf"));

        rbday.setTypeface(Typeface.createFromAsset(getAssets(), "font/homa.ttf"));
        rbnight.setTypeface(Typeface.createFromAsset(getAssets(), "font/homa.ttf"));

        sbsize.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar arg0) {
                // TODO Auto-generated method stub

            }


            @Override
            public void onStartTrackingTouch(SeekBar arg0) {
                // TODO Auto-generated method stub

            }


            @Override
            public void onProgressChanged(SeekBar arg0, int value, boolean arg2) {
                // TODO Auto-generated method stub

                test.setTextSize(value);

            }
        });

        sbspace.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar arg0) {
                // TODO Auto-generated method stub

            }


            @Override
            public void onStartTrackingTouch(SeekBar arg0) {
                // TODO Auto-generated method stub

            }


            @Override
            public void onProgressChanged(SeekBar arg0, int value, boolean arg2) {
                // TODO Auto-generated method stub

                test.setLineSpacing(value, 1);

            }
        });

        rbsystem.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                test.setTypeface(null);
                sfont = "system";

            }
        });

        rbnazanin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                font = Typeface.createFromAsset(getAssets(), "font/nazanin.ttf");
                test.setTypeface(font);
                sfont = "nazanin";

            }
        });

        rbhoma.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                font = Typeface.createFromAsset(getAssets(), "font/homa.ttf");
                test.setTypeface(font);
                sfont = "homa";

            }
        });

        rbkoodak.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                font = Typeface.createFromAsset(getAssets(), "font/koodak.ttf");
                test.setTypeface(font);
                sfont = "koodak";

            }
        });

        rbday.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                test.setBackgroundColor(Color.WHITE);
                test.setTextColor(Color.BLACK);
                rbday.setChecked(true);
                mode = "day";

            }
        });

        rbnight.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                test.setBackgroundColor(Color.BLACK);
                test.setTextColor(Color.WHITE);
                rbnight.setChecked(true);
                mode = "night";
                ;

            }
        });

        save.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                sp = getApplicationContext().getSharedPreferences("setting", 0);

                Editor edit = sp.edit();
                edit.putString("font", sfont);
                if (sfont.equals("system")) {
                    kh_bime.font = null;
                } else {
                    kh_bime.font = Typeface.createFromAsset(getAssets(), "font/" + sfont + ".ttf");
                }

                edit.putInt("size", sbsize.getProgress());
                kh_bime.size = sbsize.getProgress();
                edit.putInt("space", sbspace.getProgress());
                kh_bime.space = sbspace.getProgress();
                edit.putString("mode", mode);
                kh_bime.mode = mode;
                edit.commit();

                Toast.makeText(getApplicationContext(), "تنظیمات با موفقیت ثبت شد", Toast.LENGTH_LONG).show();
                finish();

            }
        });

        cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                finish();

            }
        });

    }


    private void load() {

        sp = getApplicationContext().getSharedPreferences("setting", 0);

        //font
        String g = sp.getString("font", "homa");
        sfont = g;
        if (g.equals("system")) {

            test.setTypeface(null);
            rbsystem.setChecked(true);
        } else {

            font = Typeface.createFromAsset(getAssets(), "font/" + g + ".ttf");
            test.setTypeface(font);
            if (g.equals("nazanin")) {
                rbnazanin.setChecked(true);
            }
            if (g.equals("homa")) {
                rbhoma.setChecked(true);
            }
            if (g.equals("koodak")) {
                rbkoodak.setChecked(true);
            }

            //size
            int f = sp.getInt("size", 18);
            test.setTextSize(f);
            sbsize.setProgress(f);

            //space
            int p = sp.getInt("space", 2);
            test.setLineSpacing(p, 1);
            sbspace.setProgress(f);

            //mode
            String m = sp.getString("mode", "day");
            mode = m;
            if (m.equals("day")) {

                test.setBackgroundColor(Color.WHITE);
                test.setTextColor(Color.BLACK);
                rbday.setChecked(true);
                mode = "day";

            } else {

                test.setBackgroundColor(Color.BLACK);
                test.setTextColor(Color.WHITE);
                rbnight.setChecked(true);
                mode = "night";
            }

        }

    }


    private void set() {

        rbsystem = (RadioButton) findViewById(R.id.setting_rb_system);
        rbnazanin = (RadioButton) findViewById(R.id.setting_rb_nazanin);
        rbhoma = (RadioButton) findViewById(R.id.setting_rb_homa);
        rbkoodak = (RadioButton) findViewById(R.id.setting_koodak);

        rbday = (RadioButton) findViewById(R.id.setting_rb_day);
        rbnight = (RadioButton) findViewById(R.id.setting_rb_night);

        test = (TextView) findViewById(R.id.setting_testtext);
        s1 = (TextView) findViewById(R.id.setting1);
        s2 = (TextView) findViewById(R.id.setting2);
        s3 = (TextView) findViewById(R.id.setting3);
        s4 = (TextView) findViewById(R.id.setting4);

        sbsize = (SeekBar) findViewById(R.id.setting_sb_size);
        sbspace = (SeekBar) findViewById(R.id.setting_sb_space);

        save = (ImageView) findViewById(R.id.setting_save);
        cancel = (ImageView) findViewById(R.id.setting_cancel);
    }

}
