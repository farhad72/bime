package com.androidbegin.sidemenutabstutorial;

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;


public class ghanon extends ListActivity implements OnQueryTextListener {

    private database db;
    private String[] Name;
    private String[] Tedad; ;
    private byte[]   p;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ghanon);
        ListView lv = (ListView) findViewById(android.R.id.list);
        lv.setTextFilterEnabled(true);
        getOverflowMenu();

        db = new database(this);
        db.useable();
        refresh();

    }


    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {

        Intent i = new Intent(ghanon.this, show_matn_ghanon.class);
        i.putExtra("ghanon1", Name[position]);

        startActivity(i);
    }


    class AA extends ArrayAdapter<String> {

        public AA() {
            super(ghanon.this, R.layout.row, Name);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater in = getLayoutInflater();
            View row = in.inflate(R.layout.row, parent, false);
            TextView name = (TextView) row.findViewById(R.id.tex_ghanon);
            ImageView img = (ImageView) row.findViewById(R.id.image_ghanon);
            name.setText(Name[position]);
            db.open();
            p = db.getpic("ghanon", position + 1);
            Bitmap pm = BitmapFactory.decodeByteArray(p, 0, p.length);
            img.setImageBitmap(pm);
            db.close();

            return row;
        }

    }


    private void refresh() {

        db.open();
        int s = db.count_inbox("ghanon");
        Name = new String[s];
        for (int i = 0; i < s; i++) {
            Name[i] = db.show_titer("ghanon", i);

        }
        setListAdapter(new AA());
        db.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actions, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onQueryTextChange(String newText) {
        //  ghanon.this..getFilter().filter(newText);
        db.open();
        int s = 1;
        Name = new String[s];
        for (int i = 0; i < s; i++) {
            Name[i] = db.show_titer("ghanon", i);
        }
        setListAdapter(new AA());
        db.close();

        return false;
    }


    @Override
    public boolean onQueryTextSubmit(String arg0) {
        // TODO Auto-generated method stub
        return false;
    }


    private void getOverflowMenu() {

        try {

            ViewConfiguration config = ViewConfiguration.get(this);
            java.lang.reflect.Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

}