package com.androidbegin.sidemenutabstutorial;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class iran_matn extends Activity {

    private Button next, pre;
    private TextView titr, matn, page;

    private int      page1;
    private int      page2 = 1;
    private database db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.iran_matn);
        db = new database(this);
        db.useable();

        next = (Button) findViewById(R.id.main_matn_next);
        pre = (Button) findViewById(R.id.main_matn_pve);
        titr = (TextView) findViewById(R.id.main_matn_titr);
        matn = (TextView) findViewById(R.id.main_matn_matn);
        page = (TextView) findViewById(R.id.main_matn_page);

        Bundle ex = getIntent().getExtras();
        final String sea = ex.getString("name");
        db.open();
        page1 = db.iran_page_count("irankardi_main", sea);
        db.close();

        matn.setTypeface(irankardi_main.font);
        matn.setTextSize(irankardi_main.size);
        matn.setLineSpacing(irankardi_main.space, 1);

        RelativeLayout lr = (RelativeLayout) findViewById(R.id.main_matn_Layout);
        if (irankardi_main.mode.equals("day")) {
            lr.setBackgroundColor(Color.WHITE);
            matn.setTextColor(Color.BLACK);
            titr.setTextColor(Color.BLACK);
            page.setTextColor(Color.BLACK);

        } else {

            lr.setBackgroundColor(Color.BLACK);
            matn.setTextColor(Color.WHITE);
            titr.setTextColor(Color.WHITE);
            page.setTextColor(Color.WHITE);

        }

        load(sea, page2);

        next.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (page1 == page2) {

                } else {

                    page2++;
                    load(sea, page2);
                }

            }
        });

        pre.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (page2 == 1) {

                } else {

                    page2--;
                    load(sea, page2);
                }

            }
        });
    }


    private void load(String sea, int page3) {

        db.open();
        titr.setText("اطلاعات جامع و کامل در مورد جای که شما می خواهید به انجا سفر کنید ");
        matn.setText(db.iran_main_display("irankardi_main", sea, page3 + ""));
        page.setText("صفحه" + page2 + "از" + page1);
        db.close();
    }
}
