package com.androidbegin.sidemenutabstutorial;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class irankardi_main extends ListActivity {

    private database          db;
    private String[]          Name;
    private String[]          Tedad; ;
    private byte[]            p;

    public static Typeface    font;
    public static int         size;
    public static int         space;
    public static String      mode;
    private SharedPreferences sp;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.irankardi_main);
        db = new database(this);
        db.useable();
        refresh();
        load();

    }


    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {

        Intent i = new Intent(irankardi_main.this, iran_matn.class);
        // i.putExtra("ghanon1", Name[position]);
        i.putExtra("name", Tedad[position]);
        //Toast.makeText(this, Tedad[position], Toast.LENGTH_SHORT).show();
        startActivity(i);
    }


    class AA extends ArrayAdapter<String> {

        public AA() {
            super(irankardi_main.this, R.layout.row, Name);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater in = getLayoutInflater();
            View row = in.inflate(R.layout.row, parent, false);
            TextView name = (TextView) row.findViewById(R.id.tex_ghanon);
            ImageView img = (ImageView) row.findViewById(R.id.image_ghanon);
            // TextView iran = (TextView) row.findViewById(R.id.text_iran);
            name.setText(Name[position]);
            //iran.setText(Tedad[position]);
            db.open();
            p = db.getpic_iran("irankardi_main", position + 1);
            Bitmap pm = BitmapFactory.decodeByteArray(p, 0, p.length);
            img.setImageBitmap(pm);
            db.close();

            return row;
        }

    }


    private void refresh() {

        db.open();
        int s = db.count_iran("irankardi_main", "a");
        Toast.makeText(this, "" + s, Toast.LENGTH_SHORT).show();
        Name = new String[s];
        Tedad = new String[s];
        for (int i = 0; i < s; i++) {
            Name[i] = db.iran_display("irankardi_main", i);
            // Toast.makeText(this, Name[i], Toast.LENGTH_SHORT).show();
            Tedad[i] = db.a("irankardi_main", i);
            //  Toast.makeText(this, Tedad[i], Toast.LENGTH_LONG).show();

        }
        setListAdapter(new AA());
        db.close();
    }


    private void load() {
        sp = getApplicationContext().getSharedPreferences("setting", 0);
        String f = sp.getString("font", "homa");
        font = Typeface.createFromAsset(getAssets(), "font/" + f + ".ttf");

        size = sp.getInt("size", 14);
        space = sp.getInt("space", 14);
        mode = sp.getString("mode", "day");
    }

}