package com.androidbegin.sidemenutabstutorial;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class kh_b_froshmasolat_amozeshi extends ListActivity {

    private ProgressDialog pd;

    private String         res = "";

    private String[]       name;


    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.kh_b_froshmasolat_amozeshi);

        File f = new File(Environment.getExternalStorageDirectory().toString() + "/myfile");
        if ( !f.isDirectory()) {

            f.mkdir();
        }

        pd = ProgressDialog.show(kh_b_froshmasolat_amozeshi.this, "", "Please wait ...");

        new getlist().execute();

    }


    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        // TODO Auto-generated method stub
        super.onListItemClick(l, v, position, id);

        Intent i = new Intent(kh_b_froshmasolat_amozeshi.this, svideo.class);
        i.putExtra("name", name[position]);
        startActivity(i);

    }


    private class getlist extends AsyncTask {

        @Override
        protected Object doInBackground(Object... arg0) {

            try {

                URL url = new URL("http://app.refairan.com/php/bime/getlistfile.php");
                URLConnection con = url.openConnection();

                BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                StringBuilder sb = new StringBuilder();

                String line = null;

                while ((line = reader.readLine()) != null) {

                    sb.append(line);

                }

                res = sb.toString();

            }
            catch (Exception e) {

                res = e.toString();

            }

            return "";
        }


        @SuppressWarnings("unchecked")
        @Override
        protected void onPostExecute(Object result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            name = res.split("♥");
            setListAdapter(new AA());
            pd.dismiss();
        }

    }


    private class AA extends ArrayAdapter<String> {

        public AA() {
            super(kh_b_froshmasolat_amozeshi.this, R.layout.row_vido, name);

        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater in = getLayoutInflater();
            View row = in.inflate(R.layout.row_vido, parent, false);

            TextView Name = (TextView) row.findViewById(R.id.text_vido);

            Name.setText(name[position].subSequence(0, name[position].length() - 1));

            return row;
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Toast.makeText(this, "MenuItem " + item.getTitle() + " selected.", Toast.LENGTH_SHORT).show();
        return true;
    }

}
