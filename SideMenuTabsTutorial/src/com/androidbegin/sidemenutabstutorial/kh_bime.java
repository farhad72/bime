package com.androidbegin.sidemenutabstutorial;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;


public class kh_bime extends Activity {

    public static Typeface    font;
    public static int         size;
    public static int         space;
    public static String      mode;
    private SharedPreferences sp;
    private database1         db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kh_bime);
        db = new database1(this);
        db.useable();

        load();

        ImageView kh_b_froshmasolat_amozeshi = (ImageView) findViewById(R.id.kh_b_froshmasolat_amozeshi);
        ImageView kh_b_amozesh_khsosi = (ImageView) findViewById(R.id.kh_b_amozesh_khsosi);
        ImageView kh_b_m_froshand = (ImageView) findViewById(R.id.m_froshand);
        ImageView sabt_mshakhsat = (ImageView) findViewById(R.id.sabt_mshakhsat);

        kh_b_froshmasolat_amozeshi.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                final Dialog di = new Dialog(kh_bime.this);
                di.setContentView(R.layout.dialog_m_amozshi);
                di.setTitle("محصولات اموزشی");

                ImageView online = (ImageView) di.findViewById(R.id.dialog_online);
                ImageView maghalat = (ImageView) di.findViewById(R.id.maghalat);
                ImageView BOOK = (ImageView) di.findViewById(R.id.book);

                online.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        Intent i = new Intent(kh_bime.this, kh_b_froshmasolat_amozeshi.class);
                        startActivity(i);
                        di.dismiss();

                    }
                });
                maghalat.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        startActivity(new Intent(kh_bime.this, list_season.class));
                        di.dismiss();

                    }
                });
                BOOK.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        startActivity(new Intent(kh_bime.this, book.class));
                        di.dismiss();

                    }
                });

                di.show();

            }

        });

        kh_b_amozesh_khsosi.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(kh_bime.this, kh_b_m_amozesh_khsosi.class);
                startActivity(i);

            }
        });

        kh_b_m_froshand.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(kh_bime.this, kh_b_m_froshand.class);
                startActivity(i);

            }
        });

        sabt_mshakhsat.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent i = new Intent(kh_bime.this, mshakhsat_dafater.class);
                startActivity(i);
            }
        });
    }


    private void load() {
        sp = getApplicationContext().getSharedPreferences("setting", 0);
        String f = sp.getString("font", "homa");
        font = Typeface.createFromAsset(getAssets(), "font/" + f + ".ttf");

        size = sp.getInt("size", 14);
        space = sp.getInt("space", 14);
        mode = sp.getString("mode", "day");
    }


    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {

        item.getItemId();
        switch (item.getItemId()) {
            case R.id.setting:
                Intent in = new Intent(kh_bime.this, font.class);
                startActivity(in);
                break;
            default:
                break;

        }

        return super.onOptionsItemSelected(item);
    }

}
