package com.androidbegin.sidemenutabstutorial;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


public class list_story extends ListActivity {

    private database1 db;
    private String[]  Name;
    private String[]  Tedad;
    private String    sea;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_story);
        db = new database1(this);
        Bundle ex = getIntent().getExtras();
        sea = ex.getString("sea");
        refresh();
        setListAdapter(new AA());

    }


    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {

        Intent i = new Intent(list_story.this, main_matn.class);
        i.putExtra("sea", sea);
        i.putExtra("name", Name[position]);
        i.putExtra("page", Tedad[position]);
        startActivity(i);
    }


    class AA extends ArrayAdapter<String> {

        public AA() {
            super(list_story.this, R.layout.list_story, Name);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater in = getLayoutInflater();
            View row = in.inflate(R.layout.row_story, parent, false);
            TextView name = (TextView) row.findViewById(R.id.row_story_name);
            TextView tedad = (TextView) row.findViewById(R.id.row_story_tedad);
            name.setText(Name[position]);
            tedad.setText(Tedad[position]);

            return row;
        }

    }


    private void refresh() {

        db.open();
        int s = db.Story_count("content", sea);
        Name = new String[s];
        Tedad = new String[s];

        for (int i = 0; i < s; i++) {
            Name[i] = db.Story_display("content", i, sea);
            Tedad[i] = db.Story_page_count("content", sea, Name[i].toString()) + "";

        }

        db.close();
    }

}
