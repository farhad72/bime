package com.androidbegin.sidemenutabstutorial;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class main_matn extends Activity {

    private Button    next, pre;
    private TextView  titr, matn, page;
    private String    sea;
    private String    name;
    private int       page1;
    private int       page2 = 1;

    private database1 db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_matn);

        next = (Button) findViewById(R.id.main_matn_next);
        pre = (Button) findViewById(R.id.main_matn_pve);
        titr = (TextView) findViewById(R.id.main_matn_titr);
        matn = (TextView) findViewById(R.id.main_matn_matn);
        page = (TextView) findViewById(R.id.main_matn_page);
        db = new database1(this);
        Bundle ex = getIntent().getExtras();
        sea = ex.getString("sea");
        name = ex.getString("name");
        page1 = Integer.parseInt(ex.getString("page"));

        matn.setTypeface(kh_bime.font);
        matn.setTextSize(kh_bime.size);
        matn.setLineSpacing(kh_bime.space, 1);

        RelativeLayout lr = (RelativeLayout) findViewById(R.id.main_matn_Layout);
        if (kh_bime.mode.equals("day")) {
            lr.setBackgroundColor(Color.WHITE);
            matn.setTextColor(Color.BLACK);
            titr.setTextColor(Color.BLACK);
            page.setTextColor(Color.BLACK);

        } else {

            lr.setBackgroundColor(Color.BLACK);
            matn.setTextColor(Color.WHITE);
            titr.setTextColor(Color.WHITE);
            page.setTextColor(Color.WHITE);

        }

        load(sea, name, page2);
        next.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (page1 == page2) {

                } else {

                    page2++;
                    load(sea, name, page2);
                }

            }
        });

        pre.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (page2 == 1) {

                } else {

                    page2--;
                    load(sea, name, page2);
                }

            }
        });
    }


    private void load(String sea, String name, int page3) {

        db.open();
        titr.setText(name);
        matn.setText(db.main_display("content", sea, name, page3 + ""));
        page.setText("����" + page2 + "��" + page1);
        db.close();
    }

}
