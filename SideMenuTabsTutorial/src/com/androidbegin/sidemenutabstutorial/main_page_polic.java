package com.androidbegin.sidemenutabstutorial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;


public class main_page_polic extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_page_polic);
        ImageView map = (ImageView) findViewById(R.id.P_imag1);
        ImageView img2 = (ImageView) findViewById(R.id.p_imag2);
        ImageView img3 = (ImageView) findViewById(R.id.p_image);
        ImageView img5 = (ImageView) findViewById(R.id.p_image5);
        ImageView img = (ImageView) findViewById(R.id.p_image3);

        map.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent in = new Intent(main_page_polic.this, map.class);
                startActivity(in);

            }
        });

        img2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent in = new Intent(main_page_polic.this, P_news.class);
                startActivity(in);

            }
        });

        img3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent in = new Intent(main_page_polic.this, p_10_arakhadamat.class);
                startActivity(in);

            }
        });

        img5.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent in = new Intent(main_page_polic.this, arsalnzarat.class);
                startActivity(in);

            }
        });

        img.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent in = new Intent(main_page_polic.this, p_10_talar.class);
                startActivity(in);

            }
        });

    }

}
