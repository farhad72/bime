package com.androidbegin.sidemenutabstutorial;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;


public class map extends Activity {

    private WebView webview;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);
        webview = (WebView) findViewById(R.id.map);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.loadUrl("http://www.refairan.com/app");
    }


    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.back, menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {

        item.getItemId();
        switch (item.getItemId()) {
            case R.id.back:
                finish();
                break;
            default:
                break;

        }

        return super.onOptionsItemSelected(item);
    }
}
