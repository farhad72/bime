package com.androidbegin.sidemenutabstutorial;

import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;


public class moghays extends Activity {

    private Spinner  spinner1;
    private Spinner  spinner2;
    private database db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.moghays);
        db = new database(this);
        db.useable();
        spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        ImageView btn = (ImageView) findViewById(R.id.ic_search);
        final TextView text_moghays_1 = (TextView) findViewById(R.id.text_moghays_1);
        final TextView text_moghays_2 = (TextView) findViewById(R.id.text_moghays_2);
        final TextView text_moghays_3 = (TextView) findViewById(R.id.text_moghays_3);
        final TextView text_moghays_4 = (TextView) findViewById(R.id.text_moghays_4);
        final TextView text_moghays_5 = (TextView) findViewById(R.id.text_moghays_5);
        final TextView text_moghays_6 = (TextView) findViewById(R.id.text_moghays_6);
        final TextView text_moghays_16 = (TextView) findViewById(R.id.text_moghays_16);
        final TextView text_moghays_17 = (TextView) findViewById(R.id.text_moghays_17);
        final TextView text_moghays_18 = (TextView) findViewById(R.id.text_moghays_18);

        final TextView text_moghays_7 = (TextView) findViewById(R.id.text_moghays_7);
        final TextView text_moghays_8 = (TextView) findViewById(R.id.text_moghays_8);
        final TextView text_moghays_9 = (TextView) findViewById(R.id.text_moghays_9);
        final TextView text_moghays_10 = (TextView) findViewById(R.id.text_moghays_10);
        final TextView text_moghays_11 = (TextView) findViewById(R.id.text_moghays_11);
        final TextView text_moghays_12 = (TextView) findViewById(R.id.text_moghays_12);
        final TextView text_moghays_13 = (TextView) findViewById(R.id.text_moghays_13);
        final TextView text_moghays_14 = (TextView) findViewById(R.id.text_moghays_14);
        final TextView text_moghays_15 = (TextView) findViewById(R.id.text_moghays_15);

        addItemsOnSpinner2();
        addItemsOnSpinner1();

        btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                text_moghays_1.setText("");
                text_moghays_2.setText("");
                text_moghays_3.setText("");
                text_moghays_4.setText("");
                text_moghays_5.setText("");
                text_moghays_6.setText("");
                text_moghays_16.setText("");
                text_moghays_17.setText("");
                text_moghays_18.setText("");

                text_moghays_7.setText("");
                text_moghays_8.setText("");
                text_moghays_9.setText("");
                text_moghays_10.setText("");
                text_moghays_11.setText("");
                text_moghays_12.setText("");
                text_moghays_13.setText("");
                text_moghays_14.setText("");
                text_moghays_15.setText("");
                Object s = spinner1.getSelectedItem();
                //Object s2 = spinner2.getSelectedItem();
                String m = (String) s;

                Object a = spinner2.getSelectedItem();
                //Object s2 = spinner2.getSelectedItem();
                String c = (String) a;

                if (m.equals("بیمه سامان")) {
                    int fild = 0;
                    String sea = "بیمه سامان ";
                    db.open();

                    text_moghays_7.setText(db.moghays("moghays", sea, fild + 1));
                    text_moghays_8.setText(db.moghays("moghays", sea, fild + 2));
                    text_moghays_9.setText(db.moghays("moghays", sea, fild + 3));
                    text_moghays_10.setText(db.moghays("moghays", sea, fild + 4));
                    text_moghays_11.setText(db.moghays("moghays", sea, fild + 5));
                    text_moghays_12.setText(db.moghays("moghays", sea, fild + 6));
                    text_moghays_13.setText(db.moghays("moghays", sea, fild + 7));
                    text_moghays_14.setText(db.moghays("moghays", sea, fild + 8));
                    text_moghays_15.setText(db.moghays("moghays", sea, fild + 9));
                    db.close();
                }

                if (c.equals("بیمه سامان")) {

                    int fild = 0;
                    String sea = "بیمه سامان ";
                    db.open();

                    text_moghays_1.setText(db.moghays("moghays", sea, fild + 1));
                    text_moghays_2.setText(db.moghays("moghays", sea, fild + 2));
                    text_moghays_3.setText(db.moghays("moghays", sea, fild + 3));
                    text_moghays_4.setText(db.moghays("moghays", sea, fild + 4));
                    text_moghays_5.setText(db.moghays("moghays", sea, fild + 5));
                    text_moghays_6.setText(db.moghays("moghays", sea, fild + 6));
                    text_moghays_16.setText(db.moghays("moghays", sea, fild + 7));
                    text_moghays_17.setText(db.moghays("moghays", sea, fild + 8));
                    text_moghays_18.setText(db.moghays("moghays", sea, fild + 9));
                    db.close();

                }

                if (c.equals("بیمه ما")) {

                    int fild = 0;
                    String sea = "بیمه ما ";
                    db.open();

                    text_moghays_1.setText(db.moghays("moghays", sea, fild + 1));
                    text_moghays_2.setText(db.moghays("moghays", sea, fild + 2));
                    text_moghays_3.setText(db.moghays("moghays", sea, fild + 3));
                    text_moghays_4.setText(db.moghays("moghays", sea, fild + 4));
                    text_moghays_5.setText(db.moghays("moghays", sea, fild + 5));
                    text_moghays_6.setText(db.moghays("moghays", sea, fild + 6));
                    text_moghays_16.setText(db.moghays("moghays", sea, fild + 7));
                    text_moghays_17.setText(db.moghays("moghays", sea, fild + 8));
                    text_moghays_18.setText(db.moghays("moghays", sea, fild + 9));
                    db.close();

                }

            }

        });

    }


    @SuppressWarnings("unchecked")
    public void addItemsOnSpinner2() {
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        List list = new ArrayList();
        list.add("بیمه سامان");
        list.add("بیمه ملت");
        list.add("بیمه ایران");
        list.add("بیمه کارآفرین ");
        list.add("بیمه ما");
        ArrayAdapter dataAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(dataAdapter);

    }


    // TODO Auto-generated method stub
    @SuppressWarnings("unchecked")
    public void addItemsOnSpinner1() {
        spinner1 = (Spinner) findViewById(R.id.spinner1);
        List list = new ArrayList();
        list.add("بیمه سامان");
        list.add("بیمه ملت");
        list.add("بیمه ایران");
        list.add("بیمه کارآفرین ");
        ArrayAdapter dataAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(dataAdapter);

    }

}
