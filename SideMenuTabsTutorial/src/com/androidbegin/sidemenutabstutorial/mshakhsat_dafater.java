package com.androidbegin.sidemenutabstutorial;

import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import clas.clas_sabtmshakhsat;


public class mshakhsat_dafater extends Activity {

    public static String res    = "";
    private int          server = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mshakhsat_dafater);
        final EditText sabt_name = (EditText) findViewById(R.id.sabt_name);
        final EditText sabt_namesharkat = (EditText) findViewById(R.id.sabt_namesharkat);
        final EditText sabt_addrss = (EditText) findViewById(R.id.sabt_addrss);
        final EditText sabt_number = (EditText) findViewById(R.id.sabt_number);
        final EditText sabt_email = (EditText) findViewById(R.id.sabt_email);

        Button sabt_btn = (Button) findViewById(R.id.sabt_btn);

        sabt_btn.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("unchecked")
            @Override
            public void onClick(View arg0) {
                if (find(sabt_addrss.getText().toString())) {
                    AlertDialog.Builder b = new AlertDialog.Builder(mshakhsat_dafater.this);
                    b.setMessage("شما از کاراکتر غیر مجاز |و^ استفاده کرده ایده");
                    b.setTitle("پیغام خطا");
                    b.setCancelable(false);
                    b.setNegativeButton("حذف کاراکترهای  توسط سیستم", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            sabt_addrss.setText(sabt_addrss.getText().toString().replace("|", ""));
                            sabt_addrss.setText(sabt_addrss.getText().toString().replace("^", ""));

                        }

                    });

                    b.setNeutralButton("حذف کاراکترهای توسط کاربر", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                        }
                    });
                    AlertDialog alert = b.create();
                    alert.show();

                } else {

                    if (sabt_name.getText().toString().equals("") || sabt_namesharkat.getText().toString().equals("") ||
                            sabt_addrss.getText().toString().equals("") || sabt_number.getText().toString().equals("") ||
                            sabt_email.getText().toString().equals(""))
                    {

                        Toast.makeText(getApplicationContext(), "لطفا مقدار را وارد کنید", Toast.LENGTH_SHORT).show();
                    } else
                    {
                        new clas_sabtmshakhsat("http://app.refairan.com/php/sabt_mshakhsat.php",
                                sabt_name.getText().toString(),
                                sabt_namesharkat.getText().toString(), sabt_addrss.getText().toString(),
                                sabt_number.getText().toString(), sabt_email.getText().toString()).execute();

                        final ProgressDialog pd = new ProgressDialog(mshakhsat_dafater.this);
                        pd.setMessage("در حال ارسال نظر شما  ");
                        pd.show();

                        final Timer tm = new Timer();
                        tm.scheduleAtFixedRate(new TimerTask() {

                            @Override
                            public void run() {

                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {

                                        server++;
                                        if (server == 10) {
                                            pd.cancel();
                                            tm.cancel();
                                            Toast.makeText(getApplicationContext(), "ارتیاط شما با اینترنت قطع می باشد", Toast.LENGTH_SHORT).show();

                                        }

                                        if (res.equals("ok")) {

                                            pd.cancel();
                                            tm.cancel();
                                            res = "";
                                            Toast.makeText(getApplicationContext(), "اطلاعات شما با موفقیت ثبت گردید", Toast.LENGTH_LONG).show();

                                            finish();

                                        } else if (res.equals("no")) {

                                            pd.cancel();
                                            Toast.makeText(getApplicationContext(), "خطا لطفا دوبار تلاش کنید", Toast.LENGTH_SHORT).show();

                                            finish();
                                            tm.cancel();

                                        }
                                    }

                                });

                            }
                        }, 1, 1000);
                    }

                }
            }
        });
    }


    public boolean find(String t) {

        for (int i = 0; i < t.length(); i++) {
            if (t.charAt(i) == '|' || t.charAt(i) == '^') {
                return true;
            }

        }
        return false;

    }
}