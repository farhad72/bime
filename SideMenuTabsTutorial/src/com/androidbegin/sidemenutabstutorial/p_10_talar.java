package com.androidbegin.sidemenutabstutorial;

import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import clas_p10.clas_getcount_talar;
import clas_p10.clas_p_talar;
import clas_p10.clas_talar_shbat;


public class p_10_talar extends Activity {

    private database     db;

    public static String res    = "";
    public static String sec    = "";
    public static String countS = "";
    private int          server = 0;
    private LinearLayout ll;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_10_talar);
        ImageView talar = (ImageView) findViewById(R.id.btn_talar);
        final EditText matn = (EditText) findViewById(R.id.text_talar);

        ll = (LinearLayout) findViewById(R.id.myll);
        db = new database(this);
        db.useable();
        getcount();
        //  refresh();

        talar.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("unchecked")
            @Override
            public void onClick(View arg0) {

                if (find(matn.getText().toString())) {
                    AlertDialog.Builder b = new AlertDialog.Builder(p_10_talar.this);
                    b.setMessage("شما از کاراکتر غیر مجاز |و^ استفاده کرده ایده");
                    b.setTitle("پیغام خطا");
                    b.setCancelable(false);
                    b.setNegativeButton("حذف کاراکترهای  توسط سیستم", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            matn.setText(matn.getText().toString().replace("|", ""));
                            matn.setText(matn.getText().toString().replace("^", ""));

                        }

                    });

                    b.setNeutralButton("حذف کاراکترهای توسط کاربر", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                        }
                    });
                    AlertDialog alert = b.create();
                    alert.show();

                } else {

                    if (matn.getText().toString().equals(""))
                    {

                        Toast.makeText(getApplicationContext(), "لطفا موضوع و نظر خود را وارد کنید", Toast.LENGTH_SHORT).show();
                    } else
                    {
                        new clas_talar_shbat("http://app.refairan.com/php/p+10/insert_talar.php",
                                matn.getText().toString()).execute();

                        final ProgressDialog pd = new ProgressDialog(p_10_talar.this);
                        pd.setMessage("در حال ارسال نظر شما  ");
                        pd.show();

                        final Timer tm = new Timer();
                        tm.scheduleAtFixedRate(new TimerTask() {

                            @Override
                            public void run() {

                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {

                                        server++;
                                        if (server == 10) {
                                            pd.cancel();
                                            tm.cancel();
                                            Toast.makeText(getApplicationContext(), "ارتیاط شما با اینترنت قطع می باشد", Toast.LENGTH_SHORT).show();

                                        }

                                        if (sec.equals("ok")) {

                                            pd.cancel();
                                            Toast.makeText(getApplicationContext(), "نظر شما با موافقیت ثبت گردیده", Toast.LENGTH_SHORT).show();
                                            sec = "";
                                            finish();
                                            tm.cancel();

                                        } else if (sec.equals("no")) {

                                            pd.cancel();
                                            Toast.makeText(getApplicationContext(), "خطا لطفا دوبار تلاش کنید", Toast.LENGTH_SHORT).show();

                                            finish();
                                            tm.cancel();

                                        }
                                    }

                                });

                            }
                        }, 1, 1000);
                    }

                }
            }
        });
    }


    public boolean find(String t) {

        for (int i = 0; i < t.length(); i++) {
            if (t.charAt(i) == '|' || t.charAt(i) == '^') {
                return true;
            }

        }
        return false;

    }


    private void refresh() {

        db.open();
        int b = db.count_inbox("whatsapp");

        //   Name = new String[s];
        for (int i = 0; i < b; i++) {
            String s = db.TALAR("whatsapp", i);
            ctext(s);

        }

        db.close();
    }


    private void ctext(String text) {

        TextView tv = new TextView(p_10_talar.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp.setMargins(0, 10, 0, 10);
        tv.setPadding(10, 10, 10, 10);
        // tv.setBackgroundColor(Color.parseColor("#00FF00"));
        tv.setBackgroundColor(R.drawable.sample_editbox);

        lp.gravity = Gravity.RIGHT;
        tv.setText(text);
        ll.addView(tv, lp);

    }


    @SuppressWarnings("unchecked")
    private void update(String count) {
        new clas_p_talar("http://app.refairan.com/php/p+10/p_10_talar.php", count, this).execute();
        final ProgressDialog pd = new ProgressDialog(p_10_talar.this);
        pd.setMessage("لطفا منتظر بماند");
        pd.show();
        final Timer tm = new Timer();
        tm.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        server++;
                        if (server == 5) {
                            pd.cancel();
                            tm.cancel();
                            Toast.makeText(getApplicationContext(), "ارتیاط شما با اینترنت قطع می باشد", Toast.LENGTH_SHORT).show();
                            //  txt.setText("0");

                        }

                        if ( !res.equals("")) {
                            pd.cancel();
                            Toast.makeText(getApplicationContext(), "درحال دریافت جملات", Toast.LENGTH_SHORT).show();
                            //   getcount();
                            tm.cancel();
                            refresh();
                        }
                    }

                });

            }
        }, 1, 1000);
    }


    @SuppressWarnings("unchecked")
    private void getcount() {
        new clas_getcount_talar("http://app.refairan.com/php/p+10/p_10_talar_getcount.php").execute();

        final ProgressDialog pd = new ProgressDialog(p_10_talar.this);
        pd.setMessage("لطفا منتظر بماند  ");
        pd.show();
        final Timer tm = new Timer();
        tm.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        server++;
                        if (server == 20) {
                            pd.cancel();
                            tm.cancel();
                            Toast.makeText(getApplicationContext(), "لطفا دوباره امتحان کنید ", Toast.LENGTH_SHORT).show();
                            // txt.setText("0");

                        }

                        if ( !countS.equals("")) {
                            pd.cancel();
                            db.open();
                            int countL = db.count_inbox("whatsapp");
                            db.close();
                            int s = Integer.parseInt(countS) - countL;
                            //  txt.setText(s + "");
                            // txt1.setText(newskh);

                            tm.cancel();
                            if (s > 0) {
                                String str = String.valueOf(s);
                                update(str);
                            } else {

                                Toast.makeText(getApplicationContext(), "خبر جدید نیست", Toast.LENGTH_LONG).show();
                                refresh();
                            }

                        }
                    }

                });

            }
        }, 1, 1000);
    }

}
