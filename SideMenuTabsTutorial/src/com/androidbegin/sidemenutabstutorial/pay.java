package com.androidbegin.sidemenutabstutorial;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.net.http.SslError;
import android.os.Bundle;
import android.text.Html;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;


public class pay extends Activity {

    private WebView w;


    @SuppressLint("JavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay);

        w = (WebView) findViewById(R.id.web1);

        Bundle ex = getIntent().getExtras();

        w.getSettings().setJavaScriptEnabled(true);

        w.addJavascriptInterface(new js(), "cc");

        w.getSettings().setDomStorageEnabled(true);

        w.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {

                w.loadUrl("javascript:window.cc.show(document.getElementsByTagName('html')[0].innerHTML);");

            }


            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();

            }

        });

        w.loadUrl("http://refairan.com/pay/pay.php?price=" + ex.getString("price"));

    }


    class js {

        public void show(String content) {

            String c = Html.fromHtml(content).toString();

            String[] t = c.split("-");

            if (t[0].equals("ok")) {

                Toast.makeText(pay.this, "تراکنش شما با موفقیت انجام شد. شماره تراکنش:" + t[1], Toast.LENGTH_LONG).show();
                reqestpassword.trans = t[1];
                finish();

            } else if (t[0].equals("er")) {

                Toast.makeText(pay.this, "تراکنش موفقیت امیز نبود", Toast.LENGTH_LONG).show();
                finish();
            }

        }

    }


    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

    }

}