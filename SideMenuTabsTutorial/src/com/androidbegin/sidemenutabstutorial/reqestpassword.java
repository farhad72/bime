package com.androidbegin.sidemenutabstutorial;

import java.util.Timer;
import java.util.TimerTask;
import login.class_reqastpassword;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class reqestpassword extends Activity {

    public static String trans        = "0";

    public static String resm         = "";

    private int          server;
    private String       serialnumber = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reqestpassword);
        serialnumber = serialnumber();

        final EditText family = (EditText) findViewById(R.id.family);
        final EditText user = (EditText) findViewById(R.id.user);
        final EditText password = (EditText) findViewById(R.id.password);
        final EditText sharkat = (EditText) findViewById(R.id.sharkat);
        final EditText cod = (EditText) findViewById(R.id.cod);
        final EditText email = (EditText) findViewById(R.id.email);
        final EditText phannumber = (EditText) findViewById(R.id.phannumber);
        final EditText address = (EditText) findViewById(R.id.address);
        Button sabt = (Button) findViewById(R.id.sabt);

        sabt.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("unchecked")
            @Override
            public void onClick(View arg0) {
                if (family.getText().toString().equals("") || user.getText().toString().equals("") ||
                        password.getText().toString().equals("") || sharkat.getText().toString().equals("")
                        || cod.getText().toString().equals("") || email.getText().toString().equals("") ||
                        phannumber.getText().toString().equals("") || address.getText().toString().equals("")
                        || trans.equals(""))
                {

                    Toast.makeText(getApplicationContext(), "لطفا همه مقادیر را با دقت پر کنید :", Toast.LENGTH_SHORT).show();
                } else
                {
                    if (trans.equals("0"))
                    {
                        dialog();

                    } else {

                        new class_reqastpassword("http://app.refairan.com/php/reqastpassword.php",
                                family.getText().toString(),
                                user.getText().toString(),
                                password.getText().toString(),
                                sharkat.getText().toString()
                                , cod.getText().toString(),
                                email.getText().toString(),
                                phannumber.getText().toString(),
                                address.getText().toString()
                                , serialnumber, trans).execute();

                        final ProgressDialog pd = new ProgressDialog(reqestpassword.this);
                        pd.setMessage("در حال ارسال اطلاعات شما   ");
                        pd.show();

                        final Timer tm = new Timer();
                        tm.scheduleAtFixedRate(new TimerTask() {

                            @Override
                            public void run() {

                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {

                                        server++;
                                        if (server == 10) {
                                            pd.cancel();
                                            tm.cancel();
                                            Toast.makeText(getApplicationContext(), "ارتیاط شما با اینترنت قطع می باشد", Toast.LENGTH_SHORT).show();

                                        }

                                        if (resm.equals("ok")) {

                                            pd.cancel();
                                            Toast.makeText(getApplicationContext(), "اطلاعات شما با موافقیت ثبت گردیده", Toast.LENGTH_SHORT).show();
                                            resm = "";
                                            finish();
                                            tm.cancel();

                                        } else if (resm.equals("no")) {

                                            pd.cancel();
                                            Toast.makeText(getApplicationContext(), "خطا لطفا دوبار تلاش کنید", Toast.LENGTH_SHORT).show();

                                            finish();
                                            tm.cancel();

                                        } else if (resm.equals("ut")) {

                                            pd.cancel();
                                            Toast.makeText(getApplicationContext(), "شما قبلا ثبت نام کرده اید ", Toast.LENGTH_SHORT).show();
                                            finish();
                                            tm.cancel();

                                        }

                                    }

                                });

                            }
                        }, 1, 1000);

                    }
                }

            }
        });

    }


    private void dialog() {

        final Dialog di = new Dialog(reqestpassword.this);
        di.setContentView(R.layout.menu_pay);
        di.setTitle("پرداخت");

        TextView online = (TextView) di.findViewById(R.id.dialog_online);
        TextView dl = (TextView) di.findViewById(R.id.dialog_dl);

        online.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(reqestpassword.this, pay.class);
                i.putExtra("price", "1000");
                startActivity(i);

                di.dismiss();

            }
        });

        dl.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                di.dismiss();

            }
        });

        di.show();

    }


    public String serialnumber()

    {
        TelephonyManager tManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        final String phone_serial_number = (String) tManager.getDeviceId();
        return phone_serial_number;
    }

}
