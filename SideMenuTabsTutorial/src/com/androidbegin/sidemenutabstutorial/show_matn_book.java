package com.androidbegin.sidemenutabstutorial;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;


public class show_matn_book extends Activity {

    private String   mozo;

    private database db;
    private String   s;
    private int      pos;
    private byte[]   p;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_matn_book);

        db = new database(this);
        db.useable();

        TextView text_mozo = (TextView) findViewById(R.id.text_ghanon_b);
        TextView text_matn = (TextView) findViewById(R.id.text_matn_ghanon);
        ImageView img = (ImageView) findViewById(R.id.book);
        Bundle ex = getIntent().getExtras();
        mozo = ex.getString("ghanon1");
        String post = ex.getString("ghanon2");
        int d = Integer.parseInt(post);
        db.open();
        s = db.show_matn("book", mozo);
        db.close();

        db.open();
        p = db.getpic("book", d);
        Bitmap pm = BitmapFactory.decodeByteArray(p, 0, p.length);
        img.setImageBitmap(pm);
        db.close();

        text_mozo.setText(mozo);
        text_matn.setText(s);

    }
}
