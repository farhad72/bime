package com.androidbegin.sidemenutabstutorial;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;


public class show_matn_ghanon extends Activity {

    private String   mozo;

    private database db;
    private String   s;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_matn_ghanon);

        db = new database(this);
        db.useable();

        TextView text_mozo = (TextView) findViewById(R.id.text_ghanon_b);
        TextView text_matn = (TextView) findViewById(R.id.text_matn_ghanon);
        Bundle ex = getIntent().getExtras();
        mozo = ex.getString("ghanon1");
        db.open();
        s = db.show_matn("ghanon", mozo);
        db.close();
        //  matn = ex.getString("matn");
        text_mozo.setText(mozo);
        text_matn.setText(s);

    }
}
