package com.androidbegin.sidemenutabstutorial;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;


public class show_matn_news_p extends Activity {

    private String   mozo;
    private String   matn;
    private database db;
    private String   s;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_matn_news);
        db = new database(this);
        db.useable();

        TextView text_mozo = (TextView) findViewById(R.id.text_mozo);
        TextView text_matn = (TextView) findViewById(R.id.text_matn);
        Bundle ex = getIntent().getExtras();
        mozo = ex.getString("mozo");
        db.open();
        s = db.show_matn("P_news", mozo);
        db.close();
        //  matn = ex.getString("matn");
        text_mozo.setText(mozo);
        text_matn.setText(s);

    }

}
