package com.androidbegin.sidemenutabstutorial;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class sodor extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sodor);
        Button sodor = (Button) findViewById(R.id.btn_sodor);

        sodor.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                final Dialog di = new Dialog(sodor.this);
                di.setContentView(R.layout.menu_sodor);
                di.setTitle("لطفا نوع بیمه نامه خود را انتخاب کنید ");
                di.show();

                TextView online = (TextView) di.findViewById(R.id.dialog_online);
                TextView dl = (TextView) di.findViewById(R.id.dialog_dl);
                TextView d2 = (TextView) di.findViewById(R.id.dialog_d2);
                TextView d3 = (TextView) di.findViewById(R.id.dialog_d3);
                online.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        Intent in = new Intent(sodor.this, sodor_atomobil.class);
                        startActivity(in);
                        di.dismiss();

                    }
                });

                dl.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        Intent in = new Intent(sodor.this, sodor_masolyat_sakhtman.class);
                        startActivity(in);
                        di.dismiss();

                    }
                });

                d2.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        Intent in = new Intent(sodor.this, sodor_khdamati_bazarkani.class);
                        startActivity(in);
                        di.dismiss();

                    }
                });
                d3.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        Intent in = new Intent(sodor.this, sodor_atashsoze.class);
                        startActivity(in);
                        di.dismiss();

                    }
                });

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Toast.makeText(this, "MenuItem " + item.getTitle() + " selected.", Toast.LENGTH_SHORT).show();
        return true;
    }
}
