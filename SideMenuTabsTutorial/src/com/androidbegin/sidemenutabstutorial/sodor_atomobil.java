package com.androidbegin.sidemenutabstutorial;

import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import clas.clas_sodor_atomobil;


public class sodor_atomobil extends Activity {

    public static String res    = "";
    private int          server = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sodor_atomobil);
        final EditText name_atomobil = (EditText) findViewById(R.id.name_atomobil);
        final EditText phan_atomobil = (EditText) findViewById(R.id.phan_atomobil);
        final EditText adrass_atomobil = (EditText) findViewById(R.id.adrass_atomobil);
        final EditText takhfif_atomobil = (EditText) findViewById(R.id.takhfif_atomobil);
        final EditText data_anghza_atomobil = (EditText) findViewById(R.id.data_anghza_atomobil);
        final EditText no_vasela_atomobil = (EditText) findViewById(R.id.no_vasela_atomobil);
        final Button sodor_atomobil = (Button) findViewById(R.id.sodor_atomobil);

        sodor_atomobil.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("unchecked")
            @Override
            public void onClick(View arg0) {

                if (name_atomobil.getText().toString().equals("") ||
                        phan_atomobil.getText().toString().equals("") ||
                        adrass_atomobil.getText().toString().equals("") ||
                        takhfif_atomobil.getText().toString().equals("") ||
                        data_anghza_atomobil.getText().toString().equals("") ||
                        no_vasela_atomobil.getText().toString().equals(""))
                {

                    Toast.makeText(getApplicationContext(), "لطفا مقدار را وارد کنید", Toast.LENGTH_SHORT).show();
                }

                else {

                    new clas_sodor_atomobil("http://app.refairan.com/php/bime/sodor_atomobil.php",
                            name_atomobil.getText().toString(),
                            phan_atomobil.getText().toString(),
                            adrass_atomobil.getText().toString(),
                            takhfif_atomobil.getText().toString(),
                            data_anghza_atomobil.getText().toString(),
                            no_vasela_atomobil.getText().toString()).execute();

                    final ProgressDialog pd = new ProgressDialog(sodor_atomobil.this);
                    pd.setMessage("در حال ارسال اطلاعات شما  ");
                    pd.show();

                    final Timer tm = new Timer();
                    tm.scheduleAtFixedRate(new TimerTask() {

                        @Override
                        public void run() {

                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {

                                    server++;
                                    if (server == 10) {
                                        pd.cancel();
                                        tm.cancel();
                                        Toast.makeText(getApplicationContext(), "ارتیاط شما با اینترنت قطع می باشد", Toast.LENGTH_SHORT).show();

                                    }

                                    if (res.equals("ok")) {

                                        pd.cancel();
                                        Toast.makeText(getApplicationContext(), "اطلاعات شما با موافقیت ثبت گردیده", Toast.LENGTH_SHORT).show();
                                        res = "";
                                        finish();
                                        tm.cancel();

                                    } else if (res.equals("no")) {

                                        pd.cancel();
                                        Toast.makeText(getApplicationContext(), "خطا لطفا دوبار تلاش کنید", Toast.LENGTH_SHORT).show();

                                        finish();
                                        tm.cancel();

                                    }
                                }

                            });

                        }
                    }, 1, 1000);
                }

            }

        });

    }


    public void openWhatsappContact(String number) {
        Uri uri = Uri.parse("smsto:" + number);
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.setPackage("com.whatsapp");
        startActivity(Intent.createChooser(i, ""));
    }


    public boolean find(String t) {

        for (int i = 0; i < t.length(); i++) {
            if (t.charAt(i) == '|' || t.charAt(i) == '^') {
                return true;
            }

        }
        return false;

    }

}
