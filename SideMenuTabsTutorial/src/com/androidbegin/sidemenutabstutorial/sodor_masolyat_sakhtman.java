package com.androidbegin.sidemenutabstutorial;

import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import clas.clas_masolyat_sakhtman;


public class sodor_masolyat_sakhtman extends Activity {

    public static String res    = "";
    private int          server = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sodor_masolyat_sakhtman);
        final EditText metr_sakhtman = (EditText) findViewById(R.id.metr_sakhtman);
        final EditText peshraft_sakhtman = (EditText) findViewById(R.id.peshraft_sakhtman);
        final EditText time_sakhtman = (EditText) findViewById(R.id.time_sakhtman);
        final EditText addrs_sakhtman = (EditText) findViewById(R.id.addrs_sakhtman);
        final EditText pzesh_sakhtman = (EditText) findViewById(R.id.pzesh_sakhtman);
        final EditText tedad_nafrat_sakhtman = (EditText) findViewById(R.id.no_vasela_atomobil);

        final EditText no_poshsh_sakhtman = (EditText) findViewById(R.id.no_poshsh_sakhtman);
        final EditText asklat_sakhtman = (EditText) findViewById(R.id.asklat_sakhtman);

        final Button btn_sakhtman = (Button) findViewById(R.id.btn_sakhtman);

        btn_sakhtman.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("unchecked")
            @Override
            public void onClick(View arg0) {

                if (metr_sakhtman.getText().toString().equals("") ||
                        peshraft_sakhtman.getText().toString().equals("") ||
                        time_sakhtman.getText().toString().equals("") ||
                        addrs_sakhtman.getText().toString().equals("") ||
                        pzesh_sakhtman.getText().toString().equals("") ||
                        tedad_nafrat_sakhtman.getText().toString().equals("") ||
                        no_poshsh_sakhtman.getText().toString().equals("") ||
                        asklat_sakhtman.getText().toString().equals(""))
                {

                    Toast.makeText(getApplicationContext(), "لطفا مقدار را وارد کنید", Toast.LENGTH_SHORT).show();
                }

                else {

                    new clas_masolyat_sakhtman("http://app.refairan.com/php/bime/sodor_atomobil.php",
                            metr_sakhtman.getText().toString(),
                            peshraft_sakhtman.getText().toString(),
                            time_sakhtman.getText().toString(),
                            addrs_sakhtman.getText().toString(),
                            pzesh_sakhtman.getText().toString(),
                            tedad_nafrat_sakhtman.getText().toString(),
                            no_poshsh_sakhtman.getText().toString(),
                            asklat_sakhtman.getText().toString()).execute();

                    final ProgressDialog pd = new ProgressDialog(sodor_masolyat_sakhtman.this);
                    pd.setMessage("در حال ارسال اطلاعات شما  ");
                    pd.show();

                    final Timer tm = new Timer();
                    tm.scheduleAtFixedRate(new TimerTask() {

                        @Override
                        public void run() {

                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {

                                    server++;
                                    if (server == 10) {
                                        pd.cancel();
                                        tm.cancel();
                                        Toast.makeText(getApplicationContext(), "ارتیاط شما با اینترنت قطع می باشد", Toast.LENGTH_SHORT).show();

                                    }

                                    if (res.equals("ok")) {

                                        pd.cancel();
                                        Toast.makeText(getApplicationContext(), "اطلاعات شما با موافقیت ثبت گردیده", Toast.LENGTH_SHORT).show();
                                        res = "";
                                        finish();
                                        tm.cancel();

                                    } else if (res.equals("no")) {

                                        pd.cancel();
                                        Toast.makeText(getApplicationContext(), "خطا لطفا دوبار تلاش کنید", Toast.LENGTH_SHORT).show();

                                        finish();
                                        tm.cancel();

                                    }
                                }

                            });

                        }
                    }, 1, 1000);
                }

            }

        });

    }


    public void openWhatsappContact(String number) {
        Uri uri = Uri.parse("smsto:" + number);
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.setPackage("com.whatsapp");
        startActivity(Intent.createChooser(i, ""));
    }


    public boolean find(String t) {

        for (int i = 0; i < t.length(); i++) {
            if (t.charAt(i) == '|' || t.charAt(i) == '^') {
                return true;
            }

        }
        return false;

    }

}
