package com.androidbegin.sidemenutabstutorial;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;


public class svideo extends Activity {

    private String          fname = "";
    private int             sw    = 0;
    private MediaController mc;
    private ProgressDialog  pd;

    private VideoView       v;

    int                     dlsize = 0, cdl = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.svideo);

        v = (VideoView) findViewById(R.id.svideo_vide);

        Bundle ex = getIntent().getExtras();

        fname = ex.getString("name");

        File f = new File(Environment.getExternalStorageDirectory().toString() + "/myfile");

        File fs[] = f.listFiles();

        for (int i = 0; i < fs.length; i++) {
            if (fs[i].getName().toString().equals(fname)) {
                sw = 1;
            }
        }

        if (sw == 0) {

            dialog();

        } else {

            showvideofromsd();
        }

    }


    private void showvideofromsd() {

        v.setVideoPath(Environment.getExternalStorageDirectory().toString() + "/myfile/" + fname);
        mc = new MediaController(svideo.this);
        v.setMediaController(mc);
        v.start();

    }


    private void dialog() {

        final Dialog di = new Dialog(svideo.this);
        di.setContentView(R.layout.dialog);
        di.setTitle("فایل " + fname);

        TextView online = (TextView) di.findViewById(R.id.dialog_online);
        TextView dl = (TextView) di.findViewById(R.id.dialog_dl);

        online.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                showonline();
                di.dismiss();

            }
        });

        dl.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                di.dismiss();
                pd = ProgressDialog.show(svideo.this, "", "Downloading ... 0%");

                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        fdl();
                    }
                }).start();

            }
        });

        di.show();

    }


    private void showonline() {

        try {

            pd = ProgressDialog.show(svideo.this, "", "Buffering ...", false);
            String link = "http://app.refairan.com/mainapp/" + fname.replace(" ", "%20");
            Uri l = Uri.parse(link);
            mc = new MediaController(svideo.this);
            v.setMediaController(mc);
            v.setVideoURI(l);

            v.setOnPreparedListener(new OnPreparedListener() {

                @Override
                public void onPrepared(MediaPlayer arg0) {

                    pd.dismiss();
                    v.start();

                }
            });

        }
        catch (Exception e) {

        }

    }


    private void fdl() {

        try {
            String link = "http://app.refairan.com/vido/" + fname.replace(" ", "%20");

            URL url = new URL(link);
            HttpURLConnection urlc = (HttpURLConnection) url.openConnection();

            urlc.setRequestMethod("GET");
            urlc.setDoOutput(true);
            urlc.connect();
            dlsize = urlc.getContentLength();

            File f = new File(Environment.getExternalStorageDirectory().toString() + "/myfile/" + fname);

            FileOutputStream fo = new FileOutputStream(f);

            InputStream ins = urlc.getInputStream();

            byte[] buffer = new byte[1024];

            int bl = 0;

            while ((bl = ins.read(buffer)) > 0) {

                fo.write(buffer, 0, bl);
                cdl += bl;

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        float o = ((float) cdl / dlsize) * 100;

                        pd.setMessage("Downloading ... " + o + "%");

                    }
                });

            }

            fo.close();

            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    pd.dismiss();
                    showvideofromsd();

                }
            });

        }
        catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    pd.dismiss();
                    Toast.makeText(getApplicationContext(), e.toString(), 3000).show();
                    File f = new File(Environment.getExternalStorageDirectory().toString() + "/myfile/" + fname);
                    f.delete();
                    finish();
                }
            });

        }

    }

}
