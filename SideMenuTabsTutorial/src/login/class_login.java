package login;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import android.os.AsyncTask;
import com.androidbegin.sidemenutabstutorial.Login;


public class class_login extends AsyncTask {

    private String Link = "";
    private String User = "";
    private String Pass = "";
    private String Serialnumber;


    public class_login(String link, String user, String pass, String serialnumber) {
        Link = link;
        User = user;
        Pass = pass;
        Serialnumber = serialnumber;

    }


    @Override
    protected String doInBackground(Object... arg0) {

        try {

            String data = URLEncoder.encode("user", "UTF8") + "=" + URLEncoder.encode(User, "UTF8");
            data += "&" + URLEncoder.encode("password", "UTF8") + "=" + URLEncoder.encode(Pass, "UTF8");
            data += "&" + URLEncoder.encode("serialnumber", "UTF8") + "=" + URLEncoder.encode(Serialnumber, "UTF8");
            URL mylink = new URL(Link);
            URLConnection connect = mylink.openConnection();

            connect.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(connect.getOutputStream());
            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            Login.res = sb.toString();
        }

        catch (Exception e) {

        }

        return "";
    }

}
