package login;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import android.os.AsyncTask;
import com.androidbegin.sidemenutabstutorial.reqestpassword;


public class class_reqastpassword extends AsyncTask {

    private String Link         = "";
    private String Family       = "";
    private String User         = "";
    private String Password     = "";
    private String Sharkat      = "";
    private String Cod          = "";
    private String Email        = "";
    private String Phannumber   = "";
    private String Address      = "";
    private String Serialnumber = "";
    private String Trans        = "";


    public class_reqastpassword(String link, String family, String user, String password,
                                String sharkat, String cod, String email,
                                String phannumber, String address,
                                String serialnumber, String trans) {

        Link = link;
        Family = family;
        User = user;
        Password = password;
        Sharkat = sharkat;
        Cod = cod;
        Email = email;
        Phannumber = phannumber;
        Address = address;
        Serialnumber = serialnumber;
        Trans = trans;

    }


    @Override
    protected String doInBackground(Object... arg0) {

        try {

            String data = URLEncoder.encode("family", "UTF8") + "=" + URLEncoder.encode(Family, "UTF8");
            data += "&" + URLEncoder.encode("user", "UTF8") + "=" + URLEncoder.encode(User, "UTF8");
            data += "&" + URLEncoder.encode("password", "UTF8") + "=" + URLEncoder.encode(Password, "UTF8");
            data += "&" + URLEncoder.encode("sharkat", "UTF8") + "=" + URLEncoder.encode(Sharkat, "UTF8");
            data += "&" + URLEncoder.encode("cod", "UTF8") + "=" + URLEncoder.encode(Cod, "UTF8");
            data += "&" + URLEncoder.encode("email", "UTF8") + "=" + URLEncoder.encode(Email, "UTF8");
            data += "&" + URLEncoder.encode("phannumber", "UTF8") + "=" + URLEncoder.encode(Phannumber, "UTF8");
            data += "&" + URLEncoder.encode("address", "UTF8") + "=" + URLEncoder.encode(Address, "UTF8");
            data += "&" + URLEncoder.encode("serialnumber", "UTF8") + "=" + URLEncoder.encode(Serialnumber, "UTF8");
            data += "&" + URLEncoder.encode("trans", "UTF8") + "=" + URLEncoder.encode(Trans, "UTF8");

            URL mylink = new URL(Link);
            URLConnection connect = mylink.openConnection();

            connect.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(connect.getOutputStream());
            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);

            }

            reqestpassword.resm = sb.toString();

        }

        catch (Exception e) {

        }

        return "";
    }

}
